using Application.Followers;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class FollowController : BaseApiController
{
    [HttpPost("{username}")]
    public async Task<IActionResult> Follow(string username)
    {
        return HandleResult(await Mediator.Send(new FollowToggle.Command { TargetUsername = username }));
    }

    // username is coming from our parameter, the predicate we are going to get from a query string
    [HttpGet("{username}")]
    public async Task<IActionResult> GetFollowings(string username, string predicate)
    {
        return HandleResult(await Mediator.Send(new List.Query{Username = username, Predicate = predicate}));
    }
}
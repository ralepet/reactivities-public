using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.DTOs;
using API.Services;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Infrastructure.Email;
using System.Text.Json;
using Microsoft.AspNetCore.WebUtilities;
using System.Text;

namespace API.Controllers
{
    // because we are not using MediatR, we are not going to derive from BaseApiController
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly TokenService _tokenService;
        private readonly EmailSender _emailSender;

        public AccountController(UserManager<AppUser> userManager, 
            SignInManager<AppUser> signInManager, TokenService tokenService, EmailSender emailSender)
        {
            _tokenService = tokenService;
            _emailSender = emailSender;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login(LoginDto loginDto )
        {
            // we need eager loading in order to have photos after the user login
            var user = await _userManager.Users.Include(p => p.Photos)
                .FirstOrDefaultAsync(x => x.Email ==  loginDto.Email);

            // if the email address doesn't exist the user is null
            if(user == null) return Unauthorized("Invalid email");

            // to enable our test users we are adding this line of code
            if(user.UserName == "bob" || user.UserName == "tom" || user.UserName == "jane") user.EmailConfirmed = true;

            //This will check if the email is confirmed
            if(!user.EmailConfirmed) return Unauthorized("Email not confirmed");

            // lockoutOnFailure is the third argument, which lock the user out on failure, if set to true
            var result = await _signInManager.CheckPasswordSignInAsync(user, loginDto.Password, false);

            if(result.Succeeded) 
            {
                await SetRefreshToken(user);
                return CreateUserObject(user);
            }
            // if the password is wrong, than the sign-in is not successful
            return Unauthorized("Invalid password");
        }

        [AllowAnonymous]
        [HttpPost("register")]
         public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto )
        {
            // AnyAsync - Asynchronously determines whether any element of a sequence satisfies a condition.
            if(await _userManager.Users.AnyAsync(x => x.Email == registerDto.Email))
            {
                ModelState.AddModelError("email", "Email taken");
                // ValidationProblem()  - Creates an ActionResult that produces a StatusCodes.Status400BadRequest response with validation errors from ControllerBase.ModelState
                return ValidationProblem();
            }
            if(await _userManager.Users.AnyAsync(x => x.UserName == registerDto.Username))
            {
                 ModelState.AddModelError("username", "Username taken");
                return ValidationProblem();
            }

            var user = new AppUser
            {
                DisplayName = registerDto.DisplayName,
                Email = registerDto.Email,
                UserName = registerDto.Username
            };

            //If we want to disable the email confimation for test purposes, we will use the code below
            //user.EmailConfirmed = true;

            // This will save the user to our database
            var result = await _userManager.CreateAsync(user, registerDto.Password);

            if (!result.Succeeded) return BadRequest("Problem registering user");

            //this is the origin where the request came from
            var origin = Request.Headers["origin"];
            // we are generating a token, which is going to be stored in our database
            //and when our user attempts to use that token to verify their email address, then the userManager will
            //be able to use that and compare against the token that's stored in the database
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            // But because we're going to be sending this as HTML, then we're going to need to encode this token so
            //that it doesn't get modified on its way down to the client
            token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));

            // this url is where we want the user to go when they click on the link that is sent inside the message
            // we use $ to create a template string
            var verifyUrl = $"{origin}/account/verifyEmail?token={token}&email={user.Email}";
            // we will create html inside a plain string
            var message = $"<p>Please click the below link to verify your email adress:</p><p><a href='{verifyUrl}'>Click to verfy email</a></p>";

            // we are sending the email to the user
            await _emailSender.SendEmailAsync(user.Email, "Please verify email", message);

            return Ok("Registration success - please verify email");
        }

        [AllowAnonymous]
        [HttpPost("verifyEmail")]
        // we are receiving the token and the email as query string params
        public async Task<IActionResult> VerifyEmail(string token, string email) 
        {
            var user = await _userManager.FindByEmailAsync(email);
            // we can't go further if their email adress doesn't exist in our database
            if (user == null) return Unauthorized();

            // because the token is going to be encoded when we send it to the client, we need to decode it
            var decodedTokenBytes = WebEncoders.Base64UrlDecode(token);
            var decodedToken = Encoding.UTF8.GetString(decodedTokenBytes);

            // the ConfirmEmailAsync validates that an email confirmation token matches the spcified user
            var result = await _userManager.ConfirmEmailAsync(user, decodedToken);

            if (!result.Succeeded) return BadRequest("Could not verify email adress");

            return Ok("Email confirmed, you can now login");
        }

        // If a user didn't get the email link
        [AllowAnonymous]
        [HttpGet("resendEmailConfirmationLink")]
        public async Task<IActionResult> ResendEmailConfirmationLink(string email)
        {
            //Firsltly, we need to find the user
            var user = await _userManager.FindByEmailAsync(email);

            // We need to check if the user is null
            if (user == null) return Unauthorized();

            //this is the origin where the request came from
            var origin = Request.Headers["origin"];
            // we are generating a token, which is going to be stored in our database
            //and when our user attempts to use that token to verify their email address, then the userManager will
            //be able to use that and compare against the token that's stored in the database
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            // But because we're going to be sending this as HTML, then we're going to need to encode this token so
            //that it doesn't get modified on its way down to the client
            token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));

            // this url is where we want the user to go when they click on the link that is sent inside the message
            // we use $ to create a template string
            var verifyUrl = $"{origin}/account/verifyEmail?token={token}&email={user.Email}";
            // we will create html inside a plain string
            var message = $"<p>Please click the below link to verify your email address:</p><p><a href='{verifyUrl}'>Click to verfy email</a></p>";

            // we are sending the email to the user
            await _emailSender.SendEmailAsync(user.Email, "Please verify email", message);

            return Ok("Email verification link resent");
        }

        // In order to get the user information directly from the token we need to add [Authorize]
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UserDto>> GetCurrentUser()
        {
            // User -  Gets the ClaimsPrincipal for user associated with the executing action
            // FindFirstValue - Returns the value for the first claim of the specified type, otherwise null if the claim is not present.
            var user = await _userManager.Users.Include(p => p.Photos)
                .FirstOrDefaultAsync( x => x.Email == User.FindFirstValue(ClaimTypes.Email));

            await SetRefreshToken(user); // this is optional, it will send a new token each time the user refreshes a page
            return CreateUserObject(user);
        }

        [Authorize]
        [HttpPost("refreshToken")]
        public async Task<ActionResult<UserDto>> RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];
            var user = await _userManager.Users
                .Include(r => r.RefreshTokens)
                .Include(p => p.Photos)
                .FirstOrDefaultAsync(x => x.UserName == User.FindFirstValue(ClaimTypes.Name));

            if (user == null) return Unauthorized();

            var oldToken = user.RefreshTokens.FirstOrDefault(x => x.Token == refreshToken);

            if (oldToken != null && !oldToken.IsActive) return Unauthorized();

            //if (oldToken != null) oldToken.Revoked = DateTime.UtcNow;

            return CreateUserObject(user);
        }

        //We will use this method on every request that we use to log in or register, 
        //so whenever a user takes that action the are going to get a new RefreshToken added to their account
        private async Task SetRefreshToken(AppUser user)
        {
            var refreshToken = _tokenService.GenerateRefreshToken();

            user.RefreshTokens.Add(refreshToken);

            await _userManager.UpdateAsync(user);

            var cookieOptions = new CookieOptions
            {
                // This means that our refresh token is not accessible via JavaScript
                HttpOnly = true,
                /*We don't need to access our token on the client, we only need to access this on a server.
                So what we can do is we can send our tech or a refresh token back in a cookie.
                And when it's in a cookie, that means the client is going to send up this token with every request
                and we will specify an expiry date of a cookie as well.
                 */
                Expires = DateTime.UtcNow.AddDays(7)
            };

            Response.Cookies.Append("refreshToken", refreshToken.Token, cookieOptions);
        }

        // this is a helper method
        private UserDto CreateUserObject(AppUser user)
        {
            return new UserDto
                {
                    DisplayName = user.DisplayName,
                    // we may not have any photos, so we are adding the optional chaining
                    Image = user.Photos.FirstOrDefault(x => x.IsMain)?.Url,
                    Token = _tokenService.CreateToken(user),
                    Username = user.UserName
                };
        }
        
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Extensions;
using Application.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace API.Controllers
{
    /// <summary>
    /// This is the base controller class for our app, it contains the Mediator as well as the HandleResult 
    /// and HandlePagedResult methods
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    {
        private IMediator  _mediator;

        // The null-coalescing assignment operator ??= assigns the value of its right-hand operand to its left-hand operand only if the left-hand operand evaluates to null. 
        //The ??= operator doesn't evaluate its right-hand operand if the left-hand operand evaluates to non-null.
        // ?? means that if _mediator is null, we will assign to Mediator whatever is right of the ??
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices
            .GetService<IMediator>();

        protected ActionResult HandleResult<T>(Result<T> result)
        {
            if(result == null) return NotFound();
            if(result.IsSuccess && result.Value != null)
                return Ok(result.Value);
            if(result.IsSuccess && result.Value == null)
                return NotFound();
            return BadRequest(result.Error);
        }
        
        /// If we are working with a PagedList class, i.e. if we want to paginate our results we will use this method
        protected ActionResult HandlePagedResult<T>(Result<PagedList<T>> result)
        {
            if(result == null) return NotFound();
            if (result.IsSuccess && result.Value != null)
            {
                // if we have a successful result we want to add pagination headers
                Response.AddPaginationHeader(result.Value.CurrentPage, result.Value.PageSize, result.Value.TotalCount, result.Value.TotalPages);
                return Ok(result.Value);
            }
            if(result.IsSuccess && result.Value == null)
                return NotFound();
            return BadRequest(result.Error);
            
        }
    }
}
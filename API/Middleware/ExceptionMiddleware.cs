using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Application.Core;

namespace API.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;
        // this is going to give us information whether we are in development mode or production mode
        private readonly IHostEnvironment _env;
        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger, 
            IHostEnvironment env)
        {
            _env = env;
            _logger = logger;
            _next = next;

        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                /* When a request comes in, it's simply going to pass straight through our exception
                    handling middleware and continue on to the other middleware*/
                await _next(context);
            }
             /* But if we get an exception anywhere in our application or our API request,
                then it's going to be caught by this piece of middleware and we're going to get 
                access to our exception*/
            catch (Exception ex)
            {
               
               // first we are going to log the error, and this is going to our terminal window
                _logger.LogError(ex, ex.Message);
                // we are setting the content type for what we are going to return
                context.Response.ContentType = "application/json";
                // then, we are going to set the status code 
                context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

                // we are checking if we are in development mode
                var response = _env.IsDevelopment()
                    // if we are in development mode, then we are going to send the full exception with the stack trace
                    ? new AppException(context.Response.StatusCode, ex.Message, ex.StackTrace?.ToString()) // Null-Conditional Operator
                    // if we aren't in development mode, we are going to send the status code and a message, without the stack trace
                    : new AppException(context.Response.StatusCode, "Server Error");

                // we are returning the response as Json, and we need JsonSerializerOptions for that
                // we are ensuring that the Json will be in camel case
                var options = new JsonSerializerOptions{PropertyNamingPolicy = JsonNamingPolicy.CamelCase};

                var json = JsonSerializer.Serialize(response, options);

                await context.Response.WriteAsync(json);
 
            }
        }
    }
}
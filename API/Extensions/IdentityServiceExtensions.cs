using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Services;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Persistence;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Infrastructure.Security;
using Microsoft.AspNetCore.Authorization;

namespace API.Extensions
{
    public static class IdentityServiceExtensions
    {
        public static IServiceCollection AddIdentityServices(this IServiceCollection services, 
            IConfiguration config) 
        {
            // Adds and configures the identity system for the specified User type
            services.AddIdentityCore<AppUser>(opt => {
                opt.Password.RequireNonAlphanumeric = false;
                // This will allow only users with confirmed email to sing in
                // We can disable this for testing purposes
                opt.SignIn.RequireConfirmedEmail = true;
            })
            //Adds an Entity Framework implementation of identity information stores.
            .AddEntityFrameworkStores<DataContext>()
            //Adds a SignInManager<TUser> for the IdentityBuilder.UserType.
            .AddSignInManager<SignInManager<AppUser>>()
            // This will allow us to create tokens for things like a confirmed email account
            // When a user regeisters we will send them an email with an token that they can use to verify that the email belongs to them
            .AddDefaultTokenProviders();

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["TokenKey"])); // config["TokenKey"] -> object property accessor

            //Registers services required by authentication services.
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer( opt => {
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true, // if this is set to false, our token doesn't do anything
                        IssuerSigningKey = key,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                    //we need to configure this because in SignalR we don't have the option to send an Http authorization header
                    // our JWT is going to go up as a query string
                    opt.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            // spelling is important here because signal are the client side is going to pass our token in a query string
                            // and the key for that query string is going to be spelt exactly as this ["access_token"]
                            var accessToken = context.Request.Query["access_token"];
                            var path = context.HttpContext.Request.Path;
                            // StartsWithSegments("/chat") -> "/chat" is our endpoint for the SignalR hub
                            if (!string.IsNullOrEmpty(accessToken) && (path.StartsWithSegments("/chat")))
                            {
                                // now we will have access to this token inside or context
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddAuthorization(opt => {
                opt.AddPolicy("IsActivityHost", policy => {
                   policy.Requirements.Add( new IsHostRequirement());
                });
            });
            // this service will only last as long as the method is running
            services.AddTransient<IAuthorizationHandler, IsHostRequirementHandler>();

            // The token service is available when we inject it into our AccountController.
            // It's going to be scoped to the lifetime of the request, the HTTP request to our API
            services.AddScoped<TokenService>();

            return services;
        }
    }
}
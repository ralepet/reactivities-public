using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace API.Extensions;

public static class HttpExtensions
{
    /// <summary>
    /// This method will add pagination headers to the http response
    /// </summary>
    /// <param name="response"></param>
    /// <param name="currentPage"></param>
    /// <param name="itemsPerPage"></param>
    /// <param name="totalItems"></param>
    /// <param name="totalPages"></param>
    public static void AddPaginationHeader(this HttpResponse response, int currentPage, int itemsPerPage,
        int totalItems, int totalPages)
    {
        // we will use an anonymous object
        var paginationHeader = new
        {
            currentPage,
            itemsPerPage,
            totalItems,
            totalPages
        };
        
        // we will add the object to our headers, we need to serialize our response as Json
        response.Headers.Add("Pagination", JsonSerializer.Serialize(paginationHeader));
    }
}
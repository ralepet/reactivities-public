using Application.Activities;

var builder = WebApplication.CreateBuilder(args);

// add services to container

builder.Services.AddControllers(opt =>
{
    var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
    opt.Filters.Add(new AuthorizeFilter(policy));
});

builder.Services.AddValidatorsFromAssemblyContaining<Create>();
// we can access the configuration via the builder variable
builder.Services.AddApplicationServices(builder.Configuration);
builder.Services.AddIdentityServices(builder.Configuration);

// Configure the http request pipeline

// we need to get a refrenece to our app
var app = builder.Build();

// we are adding middleware
app.UseMiddleware<ExceptionMiddleware>();

//These settings are going to help us with security
app.UseXContentTypeOptions();
//Our browser will not send any referer information
app.UseReferrerPolicy(opt => opt.NoReferrer());
// This will give us cross site scripting protection
app.UseXXssProtection(opt => opt.EnabledWithBlockMode());
//This will prevent our app from being used in an iframe somewhere
app.UseXfo(opt => opt.Deny());
//This is the content security policy
app.UseCspReportOnly(opt => opt
    //We won't be able to have both http and https content on our site
    .BlockAllMixedContent()
    //This will tell the app where our sources can come from, Self means the domain where
    //the content is coming from, i.e. the published JS files, and the static content
    .StyleSources(s => s.Self().CustomSources("https://fonts.googleapis.com", "https://cdn.jsdelivr.net",
        "sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=", "sha256-tVFibyLEbUGj+pO/ZSi96c01jJCvzWilvI5Th+wLeGE="))
    //Same goes to font sources
    .FontSources(s => s.Self().CustomSources("https://fonts.gstatic.com", "data:", "https://cdn.jsdelivr.net"))
    .FormActions(s => s.Self())
    .FrameAncestors(s => s.Self())
    .ImageSources(s => s.Self().CustomSources("https://res.cloudinary.com"))
    .ScriptSources(s => s.Self())
);

// we can use the app variable to get the environment
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebAPIv5 v1"));
}
else
{
    app.Use(async (context, next) =>
    {
        //3153600 is one year
        context.Response.Headers.Add("Strict-Transport-Security", "max-age=3153600");
        await next.Invoke(); //this will call the next piece of middleware
    });
}

//app.UseHttpsRedirection();

// app routing is implicitly enabled

//default files is going to look for anything inside our wwwroot folder that is called index.html
app.UseDefaultFiles();
//this command is going to serve static files from wwwroot folder
app.UseStaticFiles();

app.UseCors("CorsPolicy");

// UseAuthentication has to be before UseAuthorization
app.UseAuthentication();
app.UseAuthorization();

// we don't need to use endpoins, we can acess them from the app
app.MapControllers();
//we are adding endpoints to the ChatHub
app.MapHub<ChatHub>("/chat");

//When a client reaches specified such as /activities, our API controller is only interested
//in roots that it knows about and that's anything beginning with /api or /chat
//we are providing the endpoint so that the API can deal with any roots that is not aware of
app.MapFallbackToController("Index", "Fallback");

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

// instead of host.services, we will use app.services
using var scope = app.Services.CreateScope();

var services = scope.ServiceProvider;

try
{
    var context = services.GetRequiredService<DataContext>();
    var userManager = services.GetRequiredService<UserManager<AppUser>>();
    await context.Database.MigrateAsync();
    await Seed.SeedData(context, userManager);
}
catch (Exception ex)
{
    var logger = services.GetRequiredService<ILogger<Program>>();
    logger.LogError(ex, "An error occured during migration");
}

await app.RunAsync();

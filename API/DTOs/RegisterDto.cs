using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs
{
    public class RegisterDto
    {
        [Required]
        public string DisplayName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        // (?=.*\\d) means that at least one character has to be a number
        // (?=.*[a-z]) means that at least one character has to be a lowercase letter
        // (?=.*[A-Z]) means that at least one character has to be a uppercase letter
        // .{4,8} means that our password need to be between 4 and 8 characters long
        [RegularExpression("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$", ErrorMessage ="Password must be complex")]
        public string Password { get; set; }

        [Required]
        public string Username { get; set; }
    }
}
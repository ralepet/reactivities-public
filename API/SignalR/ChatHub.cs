using Application.Comments;
using MediatR;
using Microsoft.AspNetCore.SignalR;

namespace API.SignalR;

public class ChatHub : Hub
{
    private readonly IMediator _mediator;

    public ChatHub(IMediator mediator)
    {
        _mediator = mediator;
    }

    // The difference from an API controller is that the name Of the method is important because
    // from the client side they'll be able to invoke methods inside a SignalR hub
    // our client will make a connection to this hub and then they'll will be able to invoke methods that we create inside this hub.
    public async Task SendComment(Create.Command command)
    {
        // So what we're going to send up from the client is the properties that are contained inside the create
        //So that will be the ActivityId and the Body of the comments as well
        // Everything else we can get from our logic in our application layer
        // After the Comment has been saved saved into our database, it's going to have the CommendId, and it's going to be shaped
        // via the AutoMapper into a CommentDto
        var comment = await _mediator.Send(command);

        // we want to send the CommentDto to anybody who is connected to the hub, including the person that made the Comment
        // the way that we do that is we've got access to the connected clients inside this Clients object
        // and then we can specify who inside the Clients we want to send this to
        // the Group that we want to send it to is the one that matches the ActivityId
        await Clients.Group(command.ActivityId.ToString())
            // ReceiveComment is the name of the method, we need to use the name on the client side
            .SendAsync("ReceiveComment", comment.Value);
    }

    /// <summary>
    /// Whenever a client connects we are join them to a group of the name of the activityId and we are going to send them a
    /// list of comments that we get from the database
    /// </summary>
    public override async Task OnConnectedAsync()
    {
        var httpContext = Context.GetHttpContext();
        
        // We're going to get the activityId from the query string in this case,  we don't have root parameters
        // when we go to a SignalR hub, but we can use query string parameters to send up additional information
        // because of that spelling and case sensitivity is important here
        var activityId = httpContext.Request.Query["activityId"];
        
        //this adds a connected Client to a group, we don't need to do anything when we disconnect
        await Groups.AddToGroupAsync(Context.ConnectionId, activityId);
        
        //We are getting the list of Comments
        var result = await _mediator.Send(new List.Query { ActivityId = Guid.Parse(activityId) });
        //we are sending the list fo Comments to the person making this request to connect to our SignalR hub, the Caller in this case
        await Clients.Caller.SendAsync("LoadComments", result.Value);
    }
}
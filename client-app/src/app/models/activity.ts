import { Profile } from "./profile";

export interface Activity {
  id: string;
  title: string;
  date: Date | null;
  description: string;
  category: string;
  city: string;
  venue: string;
  hostUserName: string;
  isCancelled: boolean;
  isGoing: boolean;
  isHost: boolean;
  host?: Profile;
  attendees: Profile[];
}

// we are scafolding a new Activity object from infromation that we have from the ActivityFormValues object
export class Activity implements Activity {
  constructor(init?: ActivityFormValues) {
    // we want to take all of the properties from ActivityFormValues object and set the equivalet Activity object properties
    //the target is this, that is the Activity object itself, the source object is init which is of type ActivityFormValues
    Object.assign(this, init)
  }
}

// this is the class that we are sending to the API, it contains values from the create or edit from
export class ActivityFormValues {
  id?: string = undefined;
  title: string = '';
  category: string = '';
  description: string = '';
  date: Date | null = null;
  city: string = '';
  venue: string = '';

  // because we are using this form for both creating and updating an activity, 
  //the activity is going to be optional
  constructor(activity?: ActivityFormValues) {
    if (activity) {
      this.id = activity.id;
      this.title = activity.title;
      this.category = activity.category;
      this.description = activity.description;
      this.date = activity.date;
      this.venue = activity.venue;
      this.city = activity.city;
    }
  }
}
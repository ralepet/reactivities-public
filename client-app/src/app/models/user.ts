export interface User {
    username: string;
    displayName: string;
    token: string;
    image?: string; // image?: means that it is optional
}

export interface UserFormValues {
    email: string;
    password: string;
    displayName?: string;
    username?: string; 
}
// because semantic-ui has a Comment component, we are going to call this interface Chatcomment
export interface ChatComment {
    id: number;
    createdAt: Date;
    body: string;
    username: string;
    displayName: string;
    image: string;
}
import React, { useEffect, useState } from 'react';
import { Button, Grid, Header } from 'semantic-ui-react';
import PhotoWidgetCropper from './PhotoWidgetCropper';
import PhotoWidgetDropzone from './PhotoWidgetDropzone';

interface Props{
    loading: boolean;
    uploadPhoto: (file: Blob) => void;
}

export default function PhotoUploadWidget({loading, uploadPhoto}: Props){

    // we need to create an empty array in useState ([]), otherwise it's going to give us a warning that files is possibly undefined
    // we need to use <any> because is's going to give us a warning that property preview does not exist  on type never
    const [files, setFiles] = useState<any>([]);
    const [cropper, setCropper] = useState<Cropper>();

    function onCrop(){
        if(cropper) {
            // becaue we are sure that blob is not going to be null we are using the ! bang operator
            cropper.getCroppedCanvas().toBlob(blob => uploadPhoto(blob!));
        }
    }

    //we need to use the useEffect to clean up the component after is's disposed of
    //And the way that we use this is that we just supply a return value or a return callback.
    useEffect(() => {
        return () => {
            files.forEach((file:any) => URL.revokeObjectURL(file.preview))
        }
    }, [files])

    return (
        <Grid>
            <Grid.Column width={4}>
                <Header sub color='teal' content='Step 1 - Add Photo'/>
                <PhotoWidgetDropzone setFiles={setFiles} />
            </Grid.Column>
            <Grid.Column width={1}/> {/* Thiss Grid.Column is used for spacing*/}
            <Grid.Column width={4}>
                <Header sub color='teal' content='Step 2 - Resize Image'/>
                {files && files.length > 0 && (
                    <PhotoWidgetCropper setCropper={setCropper} imagePreview={files[0].preview}/>
                )}
            </Grid.Column>
            <Grid.Column width={1}/>
            <Grid.Column width={4}>
                <Header sub color='teal' content='Step 3 - Preview and Upload'/>
                {files && files.length > 0 && 
                <>
                    {/* The className needs to match the name of the preview property in Cropper*/ }
                    <div className='image-preview' style={{minHeight: 200, overflow:'hidden'}}/>
                    <Button.Group widths={2}>
                        <Button loading={loading} onClick={onCrop} positive icon='check'/>
                        {/* This button will set the files prop to an empty array*/ }
                        <Button disabled={loading} onClick={() => setFiles([])} icon='close'/>
                    </Button.Group>
                </>}
            </Grid.Column>
        </Grid>
    )
}
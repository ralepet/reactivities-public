import React, {useCallback} from 'react';
import {useDropzone} from 'react-dropzone';
import { Header, Icon } from 'semantic-ui-react';

interface Props {
    // we are using any instead of File as the type, because we want to add an additional property so that we can
    //add a preview to the file itself and then we'll be able to display that in our second part of our component,
    //so rather than extending the file interface and adding that property, what we'll do is just cheat and we'll use any
    setFiles: (files: any) => void; 
}

export default function PhotoWidgetDropzone({setFiles}:Props) {
    //this is the syle when the dropzone is inactive
    const dzStyles = {
        border: 'dashed 3px #eee', //#eee is dark gray
        borderColor: '#eee',
        borderRadius: '5px',
        paddingTop: '30px',
        textAlign: 'center' as 'center', // we need this to get rid of the warning
        height:200
    }
    //this is the syle when the dropzone is active
    const dzActive = {
        borderColor: 'green'
    }
    
    // useCallback will return a memoized version of the callback that only changes if one of the dependencies has changed.
    const onDrop = useCallback(acceptedFiles => {
        //  Object.assign -> Copy the values of all of the enumerable own properties from one or more source objects to a target object. Returns the target object
        setFiles(acceptedFiles.map((file : any) => Object.assign(file, { 
            preview: URL.createObjectURL(file) // this is going to give us a preview of the image that we are going to drop into the dropzone
        })))
    }, [setFiles])
    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

    return (
        // {...dzStyles, ...dzActive} the second spread operator is going to override the styles in the first
        <div {...getRootProps()} style={isDragActive ? {...dzStyles, ...dzActive} : dzStyles}>
            <input {...getInputProps()} />
            <Icon name='upload' size='huge'/>
            <Header content='Drop image here' />
        </div>
    )
}
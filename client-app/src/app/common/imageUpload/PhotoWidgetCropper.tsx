import React from 'react';
import { Cropper } from 'react-cropper';
// this will import the css for cropper, this is the only place that we are going to need it
import 'cropperjs/dist/cropper.css';

interface Props {
    imagePreview: string;
    setCropper: (cropper: Cropper) => void;
}

export default function PhotoWidgetCropper({imagePreview, setCropper} : Props){
    return(
        <Cropper
            src={imagePreview}
            // height is 200 to match the dropzone, the widht is going to be 100% to take the full width of the grid that it's contained in
            style={{height: 200, width:'100%'}}
            //we want to enforce square images, so we are going to set the initialAspectRatio and the aspectRatio to 1
            initialAspectRatio={1}
            aspectRatio={1}
            preview='.image-preview' // this gives us the option to preview cropped image, and it uses css classes to do it
            guides={false}
            viewMode={1}
            autoCropArea={1}
            background={false}
            onInitialized={cropper => setCropper(cropper)}
        />
    )
}
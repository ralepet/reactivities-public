//we can put any hooks that we create inside this file
import { useLocation } from "react-router-dom";

//whenever we create a hook we should start it with use
export default function useQuery() {
    return new URLSearchParams(useLocation().search);
}
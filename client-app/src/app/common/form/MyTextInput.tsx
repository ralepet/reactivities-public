import { useField } from 'formik';
import React from 'react';
import { Form, Label } from 'semantic-ui-react';

interface Props {
    placeholder: string;
    name: string;
    type?: string;
    label?: string; // label? means that the label is optional
}

export default function MyTextInput(props: Props) {
    // useField(props.name); is going to tie this up with the matching field that we're using for the text input.
    const [field, meta] = useField(props.name); 
    return(
        // the double !! in !!meta.error makes the object into a boolean, we need to do this because 
        // meta.error is going to be a string or undefined
        <Form.Field error={meta.touched && !!meta.error}>
            <label>{props.label}</label>
            {/* we are using ...field to get the onBlur, onChange */ }
            {/* we are using ...props to get the name , placeholder and the label in the field */ }
            <input {...field} {...props}/> 
            {meta.touched && meta.error ? (
                <Label basic color='red'>{meta.error}</Label>
            ): null}
        </Form.Field>
    )
}
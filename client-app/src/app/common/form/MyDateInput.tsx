import { useField } from 'formik';
import React from 'react';
import { Form, Label } from 'semantic-ui-react';
import DatePicker, {ReactDatePickerProps} from 'react-datepicker';


export default function MyDateInput(props: Partial<ReactDatePickerProps>) { // Partial<ReactDatePickerProps> means that every propery is optional
        // helpers allow us to manually set a value and manually set the touched status of our input component
    const [field, meta, helpers] = useField(props.name!); // we are telling our component that we know that this is going to hav a name property
    return(
        // the double !! in !!meta.error makes the object into a boolean, we need to do this because 
        // meta.error is going to be a string or undefined
        <Form.Field error={meta.touched && !!meta.error}>
           <DatePicker 
                {...field}
                {...props}
                // if we have something in the field value, then we are going to pass a Date object, otherwise we are going to pass null
                selected={(field.value && new Date(field.value)) || null}
                onChange={value => helpers.setValue(value)}
           />
            {meta.touched && meta.error ? (
                <Label basic color='red'>{meta.error}</Label>
            ): null}
        </Form.Field>
    )
}
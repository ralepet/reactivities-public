import { useField } from 'formik';
import React from 'react';
import { Form, Label, Select } from 'semantic-ui-react';

interface Props {
    placeholder: string;
    name: string;
    options: any; // because this is a select, we ar going to need a list of options
    label?: string; // label? means that the label is optional
}

export default function MySelectInput(props: Props) {
    // useField(props.name); is going to tie this up with the matching field that we're using for the text input.
    // helpers allow us to manually set a value and manually set the touched status of our input component
    const [field, meta, helpers] = useField(props.name); 
    return(
        // the double !! in !!meta.error makes the object into a boolean, we need to do this because 
        // meta.error is going to be a string or undefined
        <Form.Field error={meta.touched && !!meta.error}>
            <label>{props.label}</label>
            {/* we are using ...field to get the onBlur, onChange */ }
            {/* we are using ...props to get the name , placeholder and the label in the field */ }
            <Select 
                clearable // Using the clearable setting will let users remove their selection from a dropdown.
                options={props.options}
                value={field.value || null} // we are not spreading the properties here, because we need to be careful with the select component
                // e is event, d is data
                onChange={(e, d) => helpers.setValue(d.value)}
                onBlur={() => helpers.setTouched(true)} // we need to configure onBlur,to see if the select has been touched
                placeholder={props.placeholder}
            />
            {meta.touched && meta.error ? (
                <Label basic color='red'>{meta.error}</Label>
            ): null}
        </Form.Field>
    )
}
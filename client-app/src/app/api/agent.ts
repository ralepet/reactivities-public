import axios, { AxiosError, AxiosResponse } from "axios";
import { toast } from "react-toastify";
import { history } from "../..";
import { Activity, ActivityFormValues } from "../models/activity";
import { PaginatedResult } from "../models/pagination";
import { Photo, Profile, UserActivity } from "../models/profile";
import { User, UserFormValues } from "../models/user";
import { store } from "../stores/store";

const sleep = (delay: number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, delay)
    })
}

// this is going to pick up the value from the environment file
//if we are in development, it's going to pick up the one from env.development
// if we are in production, it's going to pick up the one from env.production
axios.defaults.baseURL = process.env.REACT_APP_API_URL;
//axios.defaults.baseURL = 'http://localhost:5000/api';

// this funcion is going to make sure that we send the token with every single request,
// when we have the token in our common store
axios.interceptors.request.use(config => {
    const token = store.commonStore.token;
    if (token) config.headers.Authorization = `Bearer ${token}`
    return config;
})

axios.interceptors.response.use(async response => {
    if(process.env.NODE_ENV === 'development')  await sleep(1000);
    // we are using the object property accessor to interogate the response
    const pagination = response.headers['pagination'];
    if(pagination) {
        response.data = new PaginatedResult(response.data, JSON.parse(pagination));
        // we need this for intellisense
        return response as AxiosResponse<PaginatedResult<any>>;
    }
    // if we don't have pagination data, then it's just a normal response
    return response;
}, (error: AxiosError) => { // anything that comes after }, axios sees as rejected
    const {data, status, config, headers} = error.response!;  // we are destructuring the error
    switch (status) {
        case 400:
            // if the data is a string we will use the toast function
            if(typeof data === 'string') {
                toast.error(data);
            }
            // if the method used in the request was get and if the data has an id property
            // then we wll redirect the user to the not found component
            if(config.method === 'get' && data.errors.hasOwnProperty('id')) {
                history.push('/not-found');
            }
            if (data.errors) { // if data contains the errors object
                const modalStateErrors = []; // we will sotre the different errors in this const
                for (const key in data.errors) {
                    if (data.errors[key]) { // [key] is the object property accessor synthax
                        modalStateErrors.push(data.errors[key])
                    }
                }
                throw modalStateErrors.flat(); // this will flatten the array, so that we will get the list of string
            } 
            break;
        case 401:
            if( status === 401 && headers['www-authenticate']?.startsWith('Bearer error ="invalid_token"')){
                store.userStore.logout();
                toast.error('Session expired - please login again')
            }
            break;
        case 404:
            history.push('/not-found');
            break;
        case 500:
            store.commonStore.setServerError(data);
            history.push('/server-error');
            break;
    }
    return Promise.reject(error);
})

// we are using generics here
const responseBody = <T>(response: AxiosResponse<T>) => response.data;

// this object is going to store the common request that we are going to make to axios
const requests = {
    get: <T>(url: string) => axios.get<T>(url).then(responseBody),
    // body: {} means that body's type is object
    post: <T>(url: string, body: {}) => axios.post<T>(url, body).then(responseBody),
    put: <T>(url: string, body: {}) => axios.put<T>(url, body).then(responseBody),
    del: <T>(url: string) => axios.delete<T>(url).then(responseBody)
}

const Activities = {
    // {params} -> we are passing the configuarion as an object, we also don't need to pass it as {params: params} because it has the same name
    // because we're not using our requests method, we also need to pass back the responseBody
    list: (params : URLSearchParams) => axios.get<PaginatedResult<Activity[]>>('/activities', {params}).then(responseBody),
    details: (id: string) => requests.get<Activity>(`/activities/${id}`),
    create: (activity: ActivityFormValues) => requests.post<void>('/activities', activity),
    update: (activity: ActivityFormValues) => requests.put<void>(`/activities/${activity.id}`, activity),
    delete: (id: string) => requests.del<void>(`/activities/${id}`),
    attend: (id: string) => requests.post<void>(`/activities/${id}/attend`,{}) // we will send an empty object {} as the body
}

const Account = {
    current: () => requests.get<User>('/account'),
    login: (user: UserFormValues) => requests.post<User>('/account/login', user),
    register: (user: UserFormValues) => requests.post<User>('/account/register', user),
    refreshToken: () => requests.post<User>('/account/refreshToken',{}),
    verifyEmail: (token: string, email: string) => requests.post<void>(`/account/verifyEmail?token=${token}&email=${email}`, {}),
    resendEmailConfirm: (email: string) => requests.get(`account/resendEmailConfirmationLink?email=${email}`)
}

const Profiles = {
    get:(username: string) => requests.get<Profile>(`/profiles/${username}`),
    // for our uploadPhoto method, we need to use Axios directly
    uploadPhoto: (file:Blob) => {
        let formData = new FormData();
        formData.append('File', file) // this needs to match the name of the property in our API
        return axios.post<Photo>('photos', formData, { // the third parameter is the config
            headers: {'Content-type': 'multipart/form-data'}
        })
    },
    setMainPhoto: (id: string) => requests.post(`/photos/${id}/setMain`, {}),
    deletePhoto: (id: string) => requests.del(`/photos/${id}`),
    updateProfile: (profile: Partial<Profile>) => requests.put(`/profiles`, profile),
    updateFollowing: (username: string) => requests.post(`/follow/${username}`, {}),
    listFollowings: (username:string, predicate:string) => requests.get<Profile[]>(`/follow/${username}?predicate=${predicate}`),
    listActivities: (username:string, predicate:string) => requests.get<UserActivity[]>(`/profiles/${username}/activities?predicate=${predicate}`)
}

const agent = {
    Activities,
    Account,
    Profiles
}

export default agent;
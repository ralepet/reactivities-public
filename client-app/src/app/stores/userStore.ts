import { makeAutoObservable, runInAction } from "mobx";
import { history } from "../..";
import agent from "../api/agent";
import { User, UserFormValues } from "../models/user";
import { store } from "./store";

export default class UserStore{
    user: User | null = null; // the inital value is going to be null
    refreshTokenTimeout: any;

    constructor() {
        makeAutoObservable(this)
    }

    // computed property, to recognize if the user is logged-in
    get isLoggedIn(){
        return !!this.user // !!this.user casts the user as a boolean
    }

    login = async (creds: UserFormValues) => {
        try {
            const user = await agent.Account.login(creds);
            // after we get our user, we will set the token that is in the common store
            store.commonStore.setToken(user.token); 
            this.startRefreshTokenTimer(user);
            // after the await, the user is available in the next tick, so if we want to modify an observable, then it has to be inside an acion
            // which means that we need to use the runInAction, rather than setting it driectly
            runInAction(() => this.user = user);
            history.push('/activities'); // we will push the user to a new location afet a successful login
            store.modalStore.closeModal();
        } catch (error) {
            throw error;
        }
    }

    logout = () => {
        store.commonStore.setToken(null);
        window.localStorage.removeItem('jwt'); // we are removing the token from the local storage
        this.user = null;
        history.push('/');// this will redirect the user to the homepage
        window.location.reload(); // I added this to reset the app, when a user logs out
    }

    getUser = async () => {
        try {
            const user = await agent.Account.current();
            store.commonStore.setToken(user.token); 
            // after the await, the user is available in the next tick, so if we want to modify an observable, then it has to be inside an acion
            // which means that we need to use the runInAction, rather than setting it driectly
            runInAction(() => this.user = user);
            this.startRefreshTokenTimer(user);
        } catch (error) {
            console.log(error);
        }
    }

    register = async( creds: UserFormValues)=> {
        try {
            await agent.Account.register(creds);
            // after the user registers, we will push them to the verify email component
            history.push(`/account/registerSuccess?email=${creds.email}`);
            store.modalStore.closeModal();
        } catch (error) {
            throw error;
        }
    }

    // this sets the image for the user
    setImage = (image: string) => {
        // because the user could be null, we need this check
        if(this.user) this.user!.image = image;
    }

    //helper method which sets the displayName fro the user
    setDisplayName = (displayName: string) => {
        // because the user could be null, we need this check
        if(this.user) this.user!.displayName = displayName;
    }

    refreshToken = async () => {
        this.stopRefreshTokenTimer();
        try {
            const user = await agent.Account.refreshToken();
            runInAction(() => this.user = user);
            // this will update our local storage
            store.commonStore.setToken(user.token);
            // once we have a new token we are going to start the timer, we need to 
            // do it whenever we recieve a token, i.e.  
            // whenever we have "store.commonStore.setToken(user.token);" in our code
            this.startRefreshTokenTimer(user);
        } catch (error) {
            console.log(error);
        }
    }

    // we want to refresh the token before it expires, so we are adding a timer
    private startRefreshTokenTimer(user: User) {
        // we want to get the contents of this token, but decode it as well, because
        // we want to check the expiry date
        // we are splitting the token by the . and because we are interested in the payload
        // we are taking the index to be 1
        const jwtToken = JSON.parse(atob(user.token.split('.')[1]));
        const expires = new Date(jwtToken.exp * 1000);
        // this is going to set our timeout value to 60 seconds before our token expires
        const timeout = expires.getTime() - Date.now() - (60 * 1000);
        this.refreshTokenTimeout = setTimeout(this.refreshToken, timeout);
    }

    private stopRefreshTokenTimer() {
        clearTimeout(this.refreshTokenTimeout);
    }
}
import { makeAutoObservable, reaction, runInAction } from "mobx";
import agent from "../api/agent";
import { Photo, Profile, UserActivity } from "../models/profile";
import { store } from "./store";

export default class ProfileStore{
    profile: Profile | null = null;
    loadingProfile = false;
    uploading = false;
    loading = false;
    followings: Profile[] = [];
    loadingFollowings = false;
    activeTab = 0;
    loadingActivities = false;
    userActivities: UserActivity[] = [];


    constructor(){
        // the properties are automatically flagged as obervables by MobX when we use  makeAutoObservable(this)
        makeAutoObservable(this);

        reaction(
            () => this.activeTab,
            activeTab => {
                // this is going to match our followings or followers
                if(activeTab === 3 || activeTab === 4){
                    const predicate = activeTab === 3 ? 'followers' : 'following';
                    this.loadFollowings(predicate);
                } else {
                     // we need this in order to reset the followings, because otherwise when a user clik back on tab3 or tab4, 
                     //there is a danger that they are going to see the previous list that doesn't match the tab that they clicked on
                    this.followings = [];
                }
            }
        )
    }

    setActiveTab = (activeTab: any) => {
        this.activeTab = activeTab;
    }

    get isCurrentUser() {
        // user -> currently logged-in user
        // profile -> profile page that the user is viewing
        if(store.userStore.user && this.profile){
         return store.userStore.user.username === this.profile.username;
        }
        return false;
    }

    // this needs to be an arrow function in order to bind it to the class
    loadProfile = async (username: string) => {
        this.loadingProfile = true;
        try {
            const profile = await agent.Profiles.get(username);
            runInAction(() => {
                this.profile = profile;
                this.loadingProfile = false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => this.loadingProfile = false);
        }
    }

    uploadPhoto = async (file: Blob) => {
        this.uploading = true;
        try {
            const response = await agent.Profiles.uploadPhoto(file);
            const photo = response.data;
            runInAction(() => {
                if(this.profile) {
                    this.profile.photos?.push(photo);
                    if(photo.isMain && store.userStore.user){
                        store.userStore.setImage(photo.url);
                        this.profile.image = photo.url;
                    }
                }
                this.uploading = false
            })
        } catch (error) {
            console.log(error);
            runInAction(() => this.uploading = false);
        }
    }

    setMainPhoto = async (photo: Photo) => {
        this.loading = true;
        try {
            await agent.Profiles.setMainPhoto(photo.id);
            //we want to update the photo in the userStore, because we are displaying it in the Navbar
            store.userStore.setImage(photo.url);
             // this will reload the activities, and fix the bug where the profile photo is changed, but displays the old photo in the 
             // activities dashboard
            store.activityStore.loadActivities();
            // we want to use runInAction because we want to update the properties in the ProfileStore itself
            runInAction(() => {
                //we want to ensure that we have this.profile and this.profile.photos
                if(this.profile && this.profile.photos){
                    //we want to find the photo that is the current main photo, and then set the isMain to false
                    this.profile.photos.find(p => p.isMain)!.isMain = false;
                    // and then we need to do the opposite for the photo that we want to set to be the main
                    this.profile.photos.find(p => p.id === photo.id)!.isMain = true;
                    //we are chainging the image of the profile
                    this.profile.image = photo.url;
                    this.loading = false;
                }
            })
        } catch (error) {
            runInAction(() => this.loading = false);
            console.log(error);
        }
    }

    deletePhoto = async (photo: Photo) => {
        this.loading = true;
        try {
            await agent.Profiles.deletePhoto(photo.id);
            runInAction(()=> {
                if(this.profile){
                    // this is going to return a new array with all of the photos except the one we just deleted in our backend
                    this.profile.photos = this.profile.photos?.filter(p => p.id !== photo.id);
                    this.loading = false
                }
            })
        } catch (error) {
            runInAction(() => this.loading = false);
            console.log(error);
        }
    }

    updateProfile = async (profile: Partial<Profile>) => {
        this.loading = true;
        try {
           await agent.Profiles.updateProfile(profile);
           runInAction(()=> {
             // we are checking if the profile has a displayname and if that name is different from the 
             // displayname of the user
            if(profile.displayName && profile.displayName !== store.userStore.user?.displayName){
                store.userStore.setDisplayName(profile.displayName);
            }

            // we will use the spread operator to override the properties that have changed
            // because profile a type of Partial<Profile>  'as Profile' to tell TypeScript that we know what we are doing
            this.profile = {...this.profile, ...profile as Profile};
            this.loading = false;
            store.activityStore.loadActivities(); // this will reload the activities in order for the bio to be updated
        })
        } catch (error) {
            runInAction(() => this.loading = false);
            console.log(error);
        }
    }

    // we need the following property in order to set the following status to, it's not the current following status
    updateFollowing = async  (username: string, following: boolean) =>  {
        this.loading = true;
        try {
            //this is calling the agent method in the api
            await agent.Profiles.updateFollowing(username);
            // we want to update the attendees as well
            store.activityStore.updateAttendeeFollowing(username);
            // because we are going to update the observables inside our profile store we need runInAction
            runInAction(() => {
                // if the profile we are looking at is not the profile of the currently logged-in user 
                // and if the profile we are looking at is the one from the parameters
                // we are going to update the followersCount, 
                //otherwise we would update the followersCount even when we click on the follow button on the ProfileCard
                if(this.profile && this.profile.username !== store.userStore.user?.username && this.profile.username === username) {
                    // following -> is the parameter, if the parameter's value is true, we are going to increment
                    // the followersCount, otherwise we are going to decrement the followersCount
                    following ? this.profile.followersCount++ : this.profile.followersCount--;
                    // we are setting the following to whatever it is not at the moment
                    this.profile.following = !this.profile.following;
                }
                // here we are changing the followingCount for the current user if they click on the follow/unfollow button on the ProfileCard of another user
                // that is either following them, or being followed by the current user
                if(this.profile && this.profile.username === store.userStore.user?.username){
                    following ? this.profile.followingCount++ : this.profile.followingCount--;
                }
                //we aslo want to update the followings in the list that we are going to store in this profile
                this.followings.forEach(profile => {
                    if(profile.username === username) {
                        // profile.following is based on the existing status, not the parameter
                        profile.following ? profile.followersCount-- : profile.followersCount++;
                        //we are setting the profile.following to whatever it is not at the moment
                        profile.following = !profile.following;
                    }
                })
                this.loading = false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => this.loading = false);
        }
    }

    // because we are going to view the user's list of followings and followers only when on their userpage, we don't need the username 
    // as a parameter, and we'll have access to their profile in this store already
    loadFollowings = async(predicate: string) => {
        this.loadingFollowings = true;
        try {
            //because we can't call this method if we aren't viewing a profile it is safe to use this.profile!.username
            const followings = await agent.Profiles.listFollowings(this.profile!.username, predicate);
            runInAction(() => {
                this.followings = followings;
                this.loadingFollowings = false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => this.loadingFollowings = false);
        }
    }

     // the predicate is going to be optional, if we don't pass it, it's going to return
     // the future activities
    loadUserActivities = async(username: string, predicate?: string) => {
        this.loadingActivities = true;
        try {
            const userActivities = await agent.Profiles.listActivities(username, predicate!);
            runInAction(() => {
                this.userActivities = userActivities;
                this.loadingActivities = false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => this.loadingActivities = false);
        }
    }
}
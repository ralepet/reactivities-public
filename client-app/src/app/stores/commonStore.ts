import { makeAutoObservable, reaction } from "mobx";
import { ServerError } from "../models/serverError";

export default class CommonStore {
    error: ServerError | null = null; // the initial value is going to be null
     // as soon as our store loads we are going to get our token from our local storage
    token: string | null = window.localStorage.getItem('jwt');
    appLoaded = false;

    constructor() {
        // becase we are passing this in the constructor, the error is now an observable
        makeAutoObservable(this); 

        // to react to a value changing we can use a reaction
        // this reaction is only called after the this.token changes
        reaction(
            () => this.token, // this is what we are reacting to
            token => { // we are passing the token as a parameter
                if(token){
                    window.localStorage.setItem('jwt', token)
                } else {
                    window.localStorage.removeItem('jwt')
                }
            }
        )
    }

    // this needs to be an arrow function, so that we don't run into any binding issues
    setServerError = (error: ServerError) => { 
        this.error = error;
    }

    setToken = (token: string | null) => {
        this.token = token;
    }

    setAppLoaded = () => {
        this.appLoaded = true;
    }
}
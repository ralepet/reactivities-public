import { HubConnection, HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { makeAutoObservable, runInAction } from "mobx";
import { ChatComment } from "../models/comment";
import { store } from "./store";

export default class CommentStore {
    comments: ChatComment[] = [];
    // HubConnection represents a connection to a SignalR Hub.
    hubConnection: HubConnection | null = null;

    constructor(){
        makeAutoObservable(this);
    }

    createHubConnection = (activityId: string) => {
        if(store.activityStore.selectedActivity){
            // this will create our connection
            this.hubConnection = new HubConnectionBuilder()
                .withUrl( process.env.REACT_APP_CHAT_URL + '?activityId=' + activityId, {
                    accessTokenFactory: () => store.userStore.user?.token!
                })
                .withAutomaticReconnect() // this will attempt to reconnect our client to the chatHub if they lose their connection
                .configureLogging(LogLevel.Information) // we want to see what is going on as we connect
                .build();
            
            // this will start our connection
            this.hubConnection.start().catch(error => console.log('Error establishing the connection: ', error));

            // when we create the connection to our hub, we want to recieve all of the comments to the Activity that they are connected to
            //this method name needs to match the one in SignalR
            // we also need the correct type information here, so we are using (comments: ChatComment[])
            this.hubConnection.on('LoadComments', (comments: ChatComment[]) => {
                // because we are updating an observable we need to call runnInAction
                runInAction(() => {
                    comments.forEach(comment => {
                        comment.createdAt = new Date(comment.createdAt); 
                    })
                    this.comments = comments
                });
            });

            //this method will be invoked when we recieve a Comment
            this.hubConnection.on('ReceiveComment', (comment: ChatComment) => {
                runInAction(() => {
                    comment.createdAt = new Date(comment.createdAt);
                    this.comments.unshift(comment);
                });
            });
        }
    }

    // this method will stop the hub connection
    stopHubconnection = () => {
        this.hubConnection?.stop().catch(error => console.log('Error stopping the connection: ', error));
    }

    // this will clear the comments when the user disconnects from the activity that they are looing at
    clearComments = () => {
        this.comments = [];
        this.stopHubconnection();
    }

    addComment = async (values: any) => {
        values.activityId = store.activityStore.selectedActivity?.id;
        try {
            //we are invoking the method directly on our server
            // this needs to match the name of the method in the chat hub
            await this.hubConnection?.invoke('SendComment', values);
        } catch (error) {
            console.log(error);
        }
    }
}
import {makeAutoObservable, reaction, runInAction} from "mobx";
import agent from "../api/agent";
import { Activity, ActivityFormValues } from "../models/activity";
import { format } from 'date-fns';
import { store } from "./store";
import { Profile } from "../models/profile";
import { Pagination, PagingParams } from "../models/pagination";

export default class ActivityStore{
    activityRegistry = new Map<string, Activity>();
    selectedActivity: Activity | undefined = undefined;
    editMode = false;
    loading = false;
    loadingInitial = false;
    pagination: Pagination | null = null;
    pagingParams = new PagingParams();
    predicate = new Map().set('all', true);

    constructor(){
        makeAutoObservable(this);

        //we don't want to react when the inital predicate is set, however, we want to react to any changes to the predicate
        reaction(
            // we want to observe and react when the keys change
            () => this.predicate.keys(), 
            () => {
                // we want to reset the paging params, because we are using filtering
                this.pagingParams = new PagingParams();
                this.activityRegistry.clear();
                // loadActivities is taking our axios params, and will load the next batch of activities
                this.loadActivities();
            }
        )
    }

    setPagingParams= (pagingParams: PagingParams) => {
        this.pagingParams = pagingParams;
    }

    setPredicate= (predicate : string, value: string | Date) => {
        //helper method to reset the predicate
        // we can use this method to switch off the other keys apart from the date
        const resetPredicate = () => {
            this.predicate.forEach((value, key) => {
                if(key !== 'startDate') this.predicate.delete(key);
            })
        }
        switch (predicate) {
            case 'all':
                resetPredicate();
                this.predicate.set('all', true);
                break;
            case 'isGoing':
                resetPredicate();
                this.predicate.set('isGoing', true);
                break;
            case 'isHost':
                resetPredicate();
                this.predicate.set('isHost', true);
                break;
            case 'startDate':
                // we are using delete this key  because of the way that we're going to react to changes in the predicate
                // if we just set the value, then we're not going to trigger the change detection to update what we're going
                // to do when the predicate changes
                this.predicate.delete('startDate');
                this.predicate.set('startDate', value);
                break;
        }
    }

    //computed property
    get axiosParams() {
        const params = new URLSearchParams();
        // we will add parameters into this object and then pass the object to axios
        params.append('pageNumber', this.pagingParams.pageNumber.toString());
        params.append('pageSize', this.pagingParams.pageSize.toString());
        //we are going to looop through our params and set any keys and values that have been
        //set as query string parameters that we can also pass up with our axios params
        this.predicate.forEach((value, key) => {
            if(key === 'startDate'){ // this is the startDate of what we want to filther by
                // because JS doesn't know what our value is supposed to be we need use '(value as Date)' and then convert it to a string
                params.append(key, (value as Date).toISOString());
            } else { // if the key is not a date, then it's a normal param
                params.append(key, value);
            }
        });
        return params;
    }

    // computed function
    // this function sorts the activities by their date
    get activitiesByDate(){
        return Array.from(this.activityRegistry.values()).sort((a, b) => 
            //getTime() return the time in miliseconds
            a.date!.getTime() - b.date!.getTime() ); // we know that the date isn't going to be null, so it's okay to use the bang operatior "!"
    }

    // computed function
    // We are going to take our list of sorted activities, and we are going to reduce this array 
    // to an object, and this object is going to have the key of each date 
    //and each date is going to have an array of the activities taking place on that date.
    get groupedActivities() {
        return Object.entries(
            this.activitiesByDate.reduce((activities, activity) => {
                // this is our key for each of the objects
                const date = format(activity.date!, 'dd MMM yyyy');
                // activities[date]  is a boolean
                activities[date] = activities[date] ? [...activities[date], activity] : [activity];
                return activities;
            }, {} as {[key: string] : Activity[]}) // this is our initial value, the key is a string
            // and the value is an array of Activities 
        )
    }

    loadActivities = async() => {
        try {
            this.loadingInitial = true;
            const result = await agent.Activities.list(this.axiosParams);
            result.data.forEach(activity => {
                this.setActivity(activity);
            })
            // we want to set the pagination when we load our activities
            this.setPagination(result.pagination);
            this.setLoadingInitial(false);

        } catch (error) {
            console.log(error);
            this.setLoadingInitial(false);
        }
    }

    //this helper method sets the pagination
    setPagination = (pagination : Pagination) => {
        this.pagination = pagination;
    }


    loadActivity = async (id: string) => {
        let activity = this.getActivity(id);
        if(activity) {
            this.selectedActivity = activity;
            return activity;
        } else {
            this.loadingInitial = true;
            try {
                activity = await agent.Activities.details(id);
                this.setActivity(activity);
                runInAction(() => {
                    this.selectedActivity = activity;
                })
                this.setLoadingInitial(false);
                return activity;
            } catch (error) {
                console.log(error);
                this.setLoadingInitial(false);
            }
        }
    }

    // helper method
    private setActivity = (activity: Activity) => {
        const user = store.userStore.user;
        // we are always going to have a user, because the setActivity cannot be invoked by someone
        // who isn't authenticated
        if(user) {
            // some -> Determines whether the specified callback function returns
            // true for any element of an array.
            activity.isGoing = activity.attendees!.some(
                a => a.username === user.username
            )
            activity.isHost = activity.hostUserName === user.username;
            activity.host = activity.attendees?.find(x => x.username === activity.hostUserName);
        }
        activity.date = new Date(activity.date!);
        this.activityRegistry.set(activity.id, activity);
    }

    // this is a helper method, which is going to return an activity or undefined
    private getActivity = (id: string) => {
        return this.activityRegistry.get(id);
    }

    setLoadingInitial = (state: boolean) => {
        this.loadingInitial = state;
    }


    createActivity = async (activity: ActivityFormValues) => {
        const user = store.userStore.user;
        const attendee = new Profile(user!);
        try {
            await agent.Activities.create(activity);
            // we are maiking an Activity object from the ActivityFormValues object using the constructor
            const newActivity = new Activity(activity);
            // because ActivityFormValues doesn't have hostUserName, we have to set that manually
            newActivity.hostUserName = user!.username;
            //we are creating a new array and passing the attendee object
            newActivity.attendees = [attendee];
            this.setActivity(newActivity);
            runInAction(() => {
                this.selectedActivity = newActivity;
            })
        } catch (error) {
            console.log(error);
        }
    }

    updateActivity = async (activity: ActivityFormValues) => {
        try {
            await agent.Activities.update(activity);
            runInAction(() => {
                // we will have the activity.id, but this will stop us from needing to use the safety operators
                if(activity.id){
                    /*We're going to need to get our activity we're passing in here,
                     keep the form values that have been updated, but also get the original other
                    information for that activity as well and combine them together */
                    let updatedActivity = {...this.getActivity(activity.id), ...activity};
                    // anything inside ...activity will override the original properties from ...this.getActivity(activity.id)
                    // so we know that we can pass the updatedActivity as a object of class Activity
                    this.activityRegistry.set(activity.id, updatedActivity as Activity);
                    this.selectedActivity = updatedActivity as Activity;
                }     
            })
        } catch (error) {
            console.log(error);
        }
    }

    deleteActivity = async (id: string) => {
        this.loading = true;
        try {
            await agent.Activities.delete(id);
            runInAction(() => {
                // this deletes the activity from the map
                this.activityRegistry.delete(id); 
                this.loading =false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => {
                this.loading =false;
            })
        }
    }

    updateAttendance = async () => {
        const user = store.userStore.user;
        this.loading = true;
        try {
            // we are using the bang operator ! because we are going to allow the user to update their 
            // attendance only if they are inside an activity
            await agent.Activities.attend(this.selectedActivity!.id);
            runInAction(() => {
                if (this.selectedActivity?.isGoing) {
                    // we are going to filter the list to remove the currently logged in user from the attendees array
                    this.selectedActivity.attendees = 
                        this.selectedActivity.attendees?.filter(a => a.username !== user?.username);
                    this.selectedActivity.isGoing = false;
                } else {
                    // we know that the user is not going to be null, so we are using the bang operator
                    const attendee = new Profile(user!);
                    // we are adding the currently logged in user to the the attendees array
                    this.selectedActivity?.attendees?.push(attendee);
                    this.selectedActivity!.isGoing = true;
                }
                // set -> Adds a new element with a specified key and value to the Map.
                //  If an element with the same key already exists, the element will be updated.
                this.activityRegistry.set(this.selectedActivity!.id, this.selectedActivity!);
            });
        } catch (error) {
            console.log(error);
        } finally {
            runInAction(() => this.loading = false);
        }
    }

    cancellActivityToggle = async () => {
        this.loading = true;
        try {
            await agent.Activities.attend(this.selectedActivity!.id);
            runInAction(() => {
                // we are setting the isCancelled to the opposite to whatever it is now
                this.selectedActivity!.isCancelled = !this.selectedActivity?.isCancelled;
                this.activityRegistry.set(this.selectedActivity!.id, this.selectedActivity!);
            })
        } catch (error) {
            console.log(error);
        } finally {
            runInAction(() => this.loading = false);
        }
    }

    clearSelectedActivity = () => {
        this.selectedActivity = undefined;
    }

    // this helper method is going to update the following status of a particular attendee
    updateAttendeeFollowing = (username: string) => {
        this.activityRegistry.forEach(activity => {
            activity.attendees.forEach(attendee => {
                // this is the username that we are about to follow/unfollow
                if(attendee.username === username) {
                    // because we are toggling between following and unfollowing, if the user is currently following this attendee, then 
                    // he is going to unfollow that attendee and we are decreasing the followersCount for that attendee
                    attendee.following ? attendee.followersCount-- : attendee.followersCount++;
                    //if the user is following the attende then he is going to unfolow and vice versa
                    attendee.following = !attendee.following;
                }
            })
        })
    }

}
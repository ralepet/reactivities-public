import React, { useEffect } from 'react';
import { Container } from 'semantic-ui-react';
import NavBar from './NavBar';
import ActivityDashboard from '../../features/activities/dashboard/ActivityDashboard';
import { observer } from 'mobx-react-lite';
import { Route, Switch, useLocation } from 'react-router-dom';
import HomePage from '../../features/home/HomePage';
import ActivityForm from '../../features/activities/form/ActivityForm';
import ActivityDetails from '../../features/activities/details/ActivityDetails';
import TestErrors from '../../features/errors/TestError';
import { ToastContainer } from 'react-toastify';
import NotFound from '../../features/errors/NotFound';
import ServerError from '../../features/errors/ServerError';
import { useStore } from '../stores/store';
import LoadingComponent from './LoadingComponents';
import ModalContainer from '../common/modals/ModalContainer';
import ProfilePage from '../../features/profiles/ProfilePage';
import PrivateRoute from './PrivateRoute';
import RegisterSuccess from '../../features/users/RegisterSuccess';
import ConfirmEmail from '../../features/users/ConfirmEmail';

function App() {
  // location is one of the props, that comes with react router
  // we are using it so that ActivityForm component re-renders  if we change form edit to create 
  const location = useLocation(); 
  const {commonStore, userStore} = useStore();

  useEffect(() => {
    if (commonStore.token){ // if we have a token
      userStore.getUser().finally(() => commonStore.setAppLoaded());
    } else {
      commonStore.setAppLoaded();
    }
  }, [commonStore, userStore]) // [commonStore, userStore] are dependencies

  if (!commonStore.appLoaded) return <LoadingComponent content='Loading app...'/>
  
  return (
    <>
      <ToastContainer position='bottom-right' hideProgressBar/>
      <ModalContainer /> {/* We need to add tihs conatainer here so that the ModalContainer is available in the entire app, just like the ToastContainer */ }
      <Route exact path='/' component={HomePage} /> 
      <Route 
       // any route that mathches the / + something else is going to match 
        // this particular route
        path={'/(.+)'} 
        render={() => (
          <>
            <NavBar />
            <Container style={{marginTop: '7em'}}>
              <Switch> {/* If we use a switch, only one route can be activated at a time*/ }
                <PrivateRoute exact path='/activities' component={ActivityDashboard} />
                <PrivateRoute path='/activities/:id' component={ActivityDetails} />
                <PrivateRoute key={location.key} path={['/createActivity', '/manage/:id']} component={ActivityForm} />
                <PrivateRoute path='/profiles/:username' component={ProfilePage} />
                <PrivateRoute path='/errors' component={TestErrors} />
                <Route path='/server-error' component={ServerError} />
                <Route path='/account/registerSuccess' component={RegisterSuccess} />
                <Route path='/account/verifyEmail' component={ConfirmEmail} />
                <PrivateRoute component={NotFound} /> {/* anything that doesn't match the routes above will hit this route*/}
              </Switch>
            </Container>
          </>
        )}
      />
    </>
  );
}

// to make our App compoent able to observe, we have to pass it in the observer function
export default observer(App); 

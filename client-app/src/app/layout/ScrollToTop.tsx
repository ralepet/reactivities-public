import { useEffect } from "react";
import { useLocation } from "react-router-dom";

export default function ScrollToTop() {
  const { pathname } = useLocation();

  //when the pathname changes, the window will be scrolled to the top
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}
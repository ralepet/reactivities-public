import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";

interface Props{
    inverted?: boolean; // inverted means that we are going to give it a light or dark background
    content?: string; // the question mark means that this is optional
}
export default function LoadingComponent({inverted = true, content = 'Loading...'}: Props) {
    return (
        <Dimmer active={true} inverted={inverted}>
            <Loader content={content}/>
        </Dimmer>
    )
}
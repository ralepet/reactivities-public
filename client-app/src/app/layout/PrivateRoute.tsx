import { Redirect, Route, RouteComponentProps, RouteProps } from "react-router-dom";
import { useStore } from "../stores/store";

// we want to pass the normal route props to the route that we are going to use
interface Props extends RouteProps {
    component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>
}

// we are passing in the rest of the properties available inside Props, which we will
// pass to the route component 
export default function PrivateRoute({component : Component, ...rest} : Props) {
    const {userStore: {isLoggedIn}} = useStore();
    return(
        <Route 
            {...rest}
            render={(props) => isLoggedIn ? <Component {...props} /> : <Redirect to='/'/>}
        />
    )
}
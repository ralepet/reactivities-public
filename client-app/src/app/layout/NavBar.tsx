import { observer } from "mobx-react-lite";
import React from "react";
import { Link, NavLink } from "react-router-dom";
import { Button, Container, Menu, Image, Dropdown } from "semantic-ui-react";
import { useStore } from "../stores/store";

export default observer (function NavBar(){ //beacause we are using the store this fucntion will be an observer
    const {userStore: {user, logout, isLoggedIn}} = useStore(); // we are destructuring the object
    return(
        <Menu inverted fixed="top">
            <Container>
                <Menu.Item as={NavLink} to='/' exact header>
                    <img src="/assets/logo.png" alt="logo"style={{marginRight: '10px'}}/>
                    Reactivities
                </Menu.Item>
                {isLoggedIn && // we want to show the rest of the navbar only to a logged in user
                <>
                    <Menu.Item as={NavLink} to='/activities' name="Activities" />
                    <Menu.Item as={NavLink} to='/errors' name="Errors" />
                    <Menu.Item>
                        <Button as={NavLink} to='/createActivity' positive content="Create Activity"/>
                    </Menu.Item>
                    <Menu.Item position='right'>
                        {/* beacuse the user could be null, we are using optinal chianing user?.image*/}
                        <Image src={user?.image || '/assets/user.png'} avatar spaced='right'/> 
                        <Dropdown pointing='top left' text={user?.displayName}>
                            <Dropdown.Menu>
                                <Dropdown.Item as={Link} to={`/profiles/${user?.username}`} text='My Profile' icon='user'/>
                                <Dropdown.Item onClick={logout} text='Logout' icon='power'/>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Menu.Item>
                </>} 
            </Container>
        </Menu>
    ) 
})
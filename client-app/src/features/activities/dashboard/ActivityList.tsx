import { observer } from "mobx-react-lite";
import React, { Fragment } from "react";
import { Header} from "semantic-ui-react";
import { useStore } from "../../../app/stores/store";
import ActivityListItem from "./ActivityListItem";

// we need the observer function because of the loading indicator
export default observer(function ActivityList() {
    const { activityStore } = useStore();
    const { groupedActivities } = activityStore;


    return (
        <>
            {groupedActivities.map(([group, activities]) => ( // group is the key, activities is the value
                <Fragment key={group}>
                    <Header sub color='teal'>
                        {group}
                    </Header>
                    {activities.map((activity) => (
                        <ActivityListItem key={activity.id} activity={activity} />
                    ))}
                </Fragment>
            ))}
        </>
    )
})
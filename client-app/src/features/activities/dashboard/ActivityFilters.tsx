import { observer } from 'mobx-react-lite';
import React from 'react';
import { Calendar } from 'react-calendar';
import { Header, Menu } from 'semantic-ui-react';
import { useStore } from '../../../app/stores/store';

// because we need to observe the values from the store, we are making this component an observer
export default observer (function ActivityFilters() {
    // we are destructuring the activityStore inline
    const {activityStore: {predicate, setPredicate}} = useStore();
    return (
        <>
            <Menu vertical size='large' style={{ width: '100%', marginTop: 26 }}>
                <Header icon='filter' attached color='teal' content='filters' />
                <Menu.Item 
                    content='All Activities'
                    active={predicate.has('all')}
                    onClick={() => setPredicate('all', 'true')}
                />
                <Menu.Item 
                    content="I'm going"
                    active={predicate.has('isGoing')}
                    onClick={() => setPredicate('isGoing', 'true')}
                />
                <Menu.Item 
                    content="I'm hosting"
                    active={predicate.has('isHost')}
                    onClick={() => setPredicate('isHost', 'true')}
                />
            </Menu>
            <Header />
        <Calendar 
            onChange={(date: any) => setPredicate('startDate', date as Date)}
            // if we don't have the date as a param, we will use new Date(), which is set to today
            value={predicate.get('starDate') || new Date()}
        />
        </>
    )
})
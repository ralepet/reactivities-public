import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { Grid, Loader } from "semantic-ui-react";
import { PagingParams } from "../../../app/models/pagination";
import { useStore } from "../../../app/stores/store";
import ActivityFilters from "./ActivityFilters";
import ActivityList from "./ActivityList";
import ActivityListItemPlaceholder from "./ActivityListItemPlaceholder";


export default observer(function ActivityDashboard() {

    const {activityStore} = useStore();
    const {loadActivities, activityRegistry, setPagingParams, pagination} = activityStore;

    //we are setting local state to see if we are loading the next batch of activities
    const [loadingNext, setLoadingNext] = useState(false);

    function handleGetnext() {
        setLoadingNext(true);
        //we want to get the next page, so we are setting the pagination to currentPage +1
        setPagingParams(new PagingParams(pagination!.currentPage +1));
        //we don't need to pass in any parameter because loadActivities is getting them from the store itself.
        // When we update our pagination params, that is going to cause our axios computed property to be updated
        // and that is what we pass to our loadActivities
        loadActivities().then(() => setLoadingNext(false));
    }

    useEffect( () => {
       if(activityRegistry.size <= 1) loadActivities();
    }, [activityRegistry.size, loadActivities]) 
  
    return(
        <Grid>
            <Grid.Column width='10'>
                {activityStore.loadingInitial && !loadingNext ? (
                    <>
                        <ActivityListItemPlaceholder />
                        <ActivityListItemPlaceholder />
                    </>
                ) : (
                    <InfiniteScroll
                        pageStart={0}
                        loadMore={handleGetnext}
                        // we need to cas the pagination into a boolean, to check if we have the pagination object
                        //when our current page is less than total pages, we do have more, otherwise we don't have more pages
                        hasMore={!loadingNext && !!pagination && pagination.currentPage < pagination.totalPages}
                        //because we are dealing with the initalLoad in our useEffect, we will set it to false
                        initialLoad={false}
                    >
                        <ActivityList />
                    </InfiniteScroll>
                )}       
            </Grid.Column>
            <Grid.Column width='6'> 
                <ActivityFilters />
            </Grid.Column>
            <Grid.Column width='10'> 
                {/* We want the user to see if we are loading something so we are adding the Loader*/}
                <Loader active={loadingNext} />
            </Grid.Column>
        </Grid>
    )
})
import { observer } from "mobx-react-lite";
import React, { useState, useEffect } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import { Button, Header, Segment } from "semantic-ui-react";
import LoadingComponent from "../../../app/layout/LoadingComponents";
import { useStore } from "../../../app/stores/store";
import {v4 as uuid} from "uuid"
import { Formik, Form } from "formik";
import * as Yup from "yup";
import MyTextInput from "../../../app/common/form/MyTextInput";
import MyTextArea from "../../../app/common/form/MyTextArea";
import MySelectInput from "../../../app/common/form/MySelectInput";
import { categoryOptions } from "../../../app/common/options/categoryOptions";
import MyDateInput from "../../../app/common/form/MyDateInput";
import { ActivityFormValues } from "../../../app/models/activity";


export default observer (function ActivityForm() {
    const history = useHistory();
    const {activityStore} = useStore();
    const  {createActivity, updateActivity, 
         loadActivity, loadingInitial} = activityStore;
    const {id} = useParams<{id: string}>();

    const [activity, setActivitiy] = useState<ActivityFormValues>(new ActivityFormValues());

    const validationSchema = Yup.object({
        title: Yup.string().required("The activity title is required"),
        description: Yup.string().required("The activity description is required"),
        category: Yup.string().required(), // .required() is going to provide us with the default message, instead of a custom one like we used before
        date: Yup.string().required("Date is required").nullable(),
        venue: Yup.string().required(),
        city: Yup.string().required()
    })

    useEffect(() => {
        if (id) loadActivity(id).then(activity => setActivitiy(new ActivityFormValues(activity))); 
        // [id, loadActivity]) are dependencies, if we don't set the dependencies, then the component will re-render, each time we set the state
        // if we add depencencies, we will execute useEffect only if the parameterc change
    }, [id, loadActivity]);

    function handleFormSubmit(activity: ActivityFormValues){
        // if we don't have an activity.id, then we are creating a new activity
        if (!activity.id) {
            let newActivity = {
                ...activity,
                id: uuid()
            };
            // we are redirecting the user using the history hook from react-router
            createActivity(newActivity).then(() => history.push(`/activities/${newActivity.id}`))
        } else {
            updateActivity(activity).then(() => history.push(`/activities/${activity.id}`))
        }
    }

    if(loadingInitial) return <LoadingComponent content='Loading activity...' />

    return(
        <Segment clearing>
            <Header content='Activity Details' sub color='teal' />
            {/* formik enableReinitialize will reset the form when new initialValues change */}
            <Formik
            validationSchema={validationSchema}
            enableReinitialize 
            initialValues={activity} 
            onSubmit={values => handleFormSubmit(values)}> 
                {({handleSubmit, isValid, isSubmitting, dirty}) => (
                <Form className='ui form' onSubmit={handleSubmit} autoComplete='off'> {/*  className='ui from' is going to give us the styling from semantic ui*/}
                    <MyTextInput  name="title" placeholder="Title"/>
                    <MyTextArea rows={3} placeholder='Description' name='description' />
                    <MySelectInput options={categoryOptions}  placeholder='Category'  name='category' />
                    <MyDateInput  
                        placeholderText='Date'  
                        name='date' 
                        showTimeSelect // this shows the time picker
                        timeCaption='time'
                        dateFormat='MMMM d, yyyy h:mm aa'
                    />
                    <Header content='Location Details' sub color='teal' />
                    <MyTextInput  placeholder='City' name='city' />
                    <MyTextInput  placeholder='Venue'  name='venue' />
                    <Button  
                        disabled={isSubmitting || !dirty || !isValid}
                        loading={isSubmitting} floated='right' 
                        positive type='submit' content='Submit'
                    />
                    <Button as={Link} to='/activities' floated='right' type='button' content='Cancel'/>
                </Form>
                )}
            </Formik>   
        </Segment>
    )
})
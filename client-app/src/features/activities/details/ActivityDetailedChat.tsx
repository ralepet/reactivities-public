import { Field, FieldProps, Form, Formik } from 'formik';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Segment, Header, Comment, Loader } from 'semantic-ui-react';
import { useStore } from '../../../app/stores/store';
import * as Yup from 'yup';
import { formatDistanceToNow } from 'date-fns';

interface Props {
    activityId: string;
}

export default observer(function ActivityDetailedChat({ activityId }: Props) {
    const { commentStore } = useStore();

    // as a side effect, when we load this component, we're going to create our hub connection
    useEffect(() => {
        // activityId is going to form the group id, so we need to check if it exists
        if (activityId) {
            commentStore.createHubConnection(activityId);
        }

        // we are adding the clean-up function so that we close down that connection when we dispose of our component
        return () => {
            commentStore.clearComments();
        }
    }, [commentStore, activityId]) // [commentStore, activityId] are the dependencies

    return (
        <>
            <Segment
                textAlign='center'
                attached='top'
                inverted
                color='teal'
                style={{ border: 'none' }}
            >
                <Header>Chat about this event</Header>
            </Segment>
            <Segment attached clearing>
                <Formik
                    // we want resetForm in order to clear our form after submit
                    onSubmit={(values, { resetForm }) => commentStore.addComment(values).then(() => resetForm())}
                    initialValues={{ body: '' }}
                    validationSchema={Yup.object({
                        body: Yup.string().required()
                    })}
                >
                    {/*isSubmitting, isValid we are getting from Formik*/}
                    {({ isSubmitting, isValid, handleSubmit }) => (
                        <Form className='ui form'> {/*/ we need this class because we are using the From from Formik, not from semantic-ui*/}
                            <Field name='body'>
                                {(props: FieldProps) => (
                                    <div style={{ position: 'relative' }}>
                                        <Loader active={isSubmitting} />
                                        <textarea
                                            placeholder='Enter your comments (Enter to submit, SHIFT + Enter for new line)'
                                            rows={2}
                                            {...props.field}
                                            onKeyPress={e => {
                                                // if someone pressed Enter and shift at the same time we don't want to sumbit
                                                if (e.key === 'Enter' && e.shiftKey) {
                                                    return;
                                                }
                                                if (e.key === 'Enter' && !e.shiftKey) {
                                                    e.preventDefault();
                                                    isValid && handleSubmit();
                                                }
                                            }}
                                        />
                                    </div>
                                )}
                            </Field>
                        </Form>
                    )}
                </Formik>
                <Comment.Group>
                    {commentStore.comments.map(comment => (
                        <Comment key={comment.id}>
                            <Comment.Avatar src={comment.image || '/assets/user.png'} />
                            <Comment.Content>
                                <Comment.Author as={Link} to={`/profiles/${comment.username}`} >{comment.displayName}</Comment.Author>
                                <Comment.Metadata>
                                    {/* formatDistanceToNow will return how long ago the comment was made*/}
                                    <div>{formatDistanceToNow(comment.createdAt)} ago</div>
                                </Comment.Metadata>
                                {/* whiteSpace: 'pre-wrap' -> Sequences of white space are preserved. 
                                Lines are broken at newline characters, at <br>, and as necessary to fill line boxes.*/ }
                                <Comment.Text style={{ whiteSpace: 'pre-wrap' }}>{comment.body}</Comment.Text>
                            </Comment.Content>
                        </Comment>
                    ))}
                </Comment.Group>
            </Segment>
        </>
    )
})
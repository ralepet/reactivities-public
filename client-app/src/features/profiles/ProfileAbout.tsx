import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import { Button, Grid, Header, Tab } from 'semantic-ui-react';
import { useStore } from '../../app/stores/store';
import ProfileEditForm from './ProfileEditForm';

// because we are using the store, we need to make this component an observer
export default observer (function ProfileAbout() {
    const {profileStore} = useStore();
    const {isCurrentUser, profile} = profileStore;
    // we need the state in order 
    const [editMode, setEditMode] = useState(false);

    return (
        <Tab.Pane>
            <Grid>
                <Grid.Column width={16}>
                    <Header floated='left' icon='user' content={`About ${profile?.displayName}`}/>
                    {isCurrentUser && (
                        <Button 
                            floated='right'
                            basic
                            content={editMode ? 'Cancel' : 'Edit Profile'}
                            // this will set the edit mode to whatever it is not at the moment
                            onClick={() => setEditMode(!editMode)}
                        />
                    )}
                </Grid.Column>
                <Grid.Column width={16}>
                {/* If the state is in edit mode then we will display the ProfileEditForm component,
                otherwise we will display the biography*/}
                {editMode ? <ProfileEditForm setEditMode={setEditMode} /> :
                    <span style={{whiteSpace: 'pre-wrap'}}>{profile?.bio}</span>}
                </Grid.Column>
            </Grid>
        </Tab.Pane>
    )
})
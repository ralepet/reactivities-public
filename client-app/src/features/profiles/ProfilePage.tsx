import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Grid } from 'semantic-ui-react';
import LoadingComponent from '../../app/layout/LoadingComponents';
import { useStore } from '../../app/stores/store';
import ProfileContent from './ProfileContent';
import ProfileHeader from './ProfileHeader';

// because we are using the pfofileStore, we need to make this component an observer
export default observer (function ProfilePage(){
    // useParams returns an object of key/value pairs of URL parameters.
    const {username} = useParams<{username: string}>(); // we are getting the username from the root param, using the React Router hook
    const {profileStore} = useStore();
    // we are destructuring the things that we need from the profileStore
    const {loadingProfile, loadProfile, profile, setActiveTab} = profileStore;

    // we need useEffect in order to call the loadProfile method when this component loads
    useEffect(() => {
        loadProfile(username);
        // we need set our active tab back to 0 so that we don't keep followings hanging around
        // after we dispose of the profile page component
        return () => {
            setActiveTab(0);
        }
    }, [loadProfile, username, setActiveTab]) // [loadProfile, username] are dependencies

    if(loadingProfile) return <LoadingComponent content='Loading profile...'/>
    return(
        <Grid>
            <Grid.Column width={16}>
                {/*because both  ProfileHeader and ProfileContent are using the profile which could be null, we are wrapping them toghether */}
                {profile &&
                <>
                    <ProfileHeader profile={profile}/>
                    <ProfileContent profile={profile}/>
                </>}
            </Grid.Column>
        </Grid>
    )
})
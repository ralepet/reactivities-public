import { observer } from 'mobx-react-lite';
import React, { SyntheticEvent } from 'react';
import { Button, Reveal } from 'semantic-ui-react';
import { Profile } from '../../app/models/profile';
import { useStore } from '../../app/stores/store';

interface Props{
    profile: Profile;
}

// because we want to connect this button to our store we are going to make it an observer
export default observer ( function FollowButton({profile} :Props){
    //we are using the userStore because we don't want to display this button if we are on the user's own profile page
    const {profileStore, userStore} = useStore();
    //we'll destructure this just for convenience
    const {updateFollowing, loading} = profileStore;

    //we won't display the button if the user is viewing their own profile
    if(userStore.user?.username === profile.username) return null;

 
    function handleFollow(e: SyntheticEvent, username: string) {
        // we need this in order to prevent the default behaviour of the button, because we are going to place this button 
        // in our ProfileCard, which acts as a Link, and this button is going to activate the link at the same time
        e.preventDefault();
        // if we are following the user, then we want to unfollow him, and we are setting the flag to false
        // if we are not following the user, we want to follow him ,and we are setting the flag to trues
        profile.following ? updateFollowing(username, false) : updateFollowing(username, true);
    }

    return (
        // In Reveal we have visible and hidden content, and when the user hovers over the button, then it displays the hidden content
        <Reveal animated='move'>
            <Reveal.Content visible style={{width: '100%'}}>
                <Button 
                fluid 
                color='teal' 
                content={profile.following ? 'Following' : 'Not following'}
                />
            </Reveal.Content>
            <Reveal.Content hidden style={{width: '100%'}}>
                <Button 
                    fluid 
                    basic
                    color={profile.following ? 'red' : 'green'}
                    content={profile.following ? 'Unfollow' : 'Follow'}
                    loading={loading}
                    onClick={(e) => handleFollow(e, profile.username)}
                />
            </Reveal.Content>
        </Reveal>
    )
})
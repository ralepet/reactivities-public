import { observer } from 'mobx-react-lite';
import React from 'react';
import { Tab } from 'semantic-ui-react';
import { Profile } from '../../app/models/profile';
import { useStore } from '../../app/stores/store';
import ProfileAbout from './ProfileAbout';
import ProfileActivities from './ProfileActivities';
import ProfileFollowings from './ProfileFollowings';
import ProfilePhotos from './ProfilePhotos';

interface Props {
    profile: Profile;
}

// even though we aren't adding a store here, because the profile object is coming from our store, we need to make this component an observer
// otherwise we won't react to any changes in our observable inside this component
export  default observer (function ProfileContent({profile}: Props){
    const {profileStore} = useStore();

    //these are the items that we are going to display in our tab
    const panes = [
        {menuItem: 'About', render: () => <ProfileAbout/>}, // ProfileAbout has tabs inside
        {menuItem: 'Photos', render: () => <ProfilePhotos profile={profile}/>}, // ProfilePhotos has a Tab.Pane inside
        {menuItem: 'Events', render: () => <ProfileActivities/>},
        {menuItem: 'Followers', render: () => <ProfileFollowings />},
        {menuItem: 'Following', render: () => <ProfileFollowings />},
    ]
    return(
        <Tab
        menu={{fluid: true, vertical: true}} 
        menuPosition='right'
        panes={panes}
        // the data is going to give us the activeIndex of the tab that was clicked
        onTabChange={(e, data) => profileStore.setActiveTab(data.activeIndex)}
        />
    )
})
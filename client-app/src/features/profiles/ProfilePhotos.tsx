import { observer } from 'mobx-react-lite';
import React, { SyntheticEvent, useState } from 'react';
import { Button, Card, Grid, Header, Image, Tab } from 'semantic-ui-react';
import PhotoUploadWidget from '../../app/common/imageUpload/PhotoUploadWidget';
import { Photo, Profile } from '../../app/models/profile';
import { useStore } from '../../app/stores/store';

interface Props {
    profile: Profile;
}

// because this component is going to recieve information from our store, we are going to make it an observer
export default observer (function ProfilePhotos({profile} : Props) {
    const {profileStore: {isCurrentUser, uploadPhoto, uploading,
        loading, setMainPhoto, deletePhoto}} = useStore();
    const [addPhotoMode, setAddPhotoMode] = useState(false); //we are adding local state, the inital value will be false
    // we want to diplay the loading indicator only on the button that we are clicking on, so we are adding the local state
    //the initial value is going to be an empty string ''
    const [target, setTarget] = useState('');

    function handlePhotoUpload(file: Blob){
        uploadPhoto(file).then(() => setAddPhotoMode(false));
    }

    function handleSetMainPhoto(photo:Photo, e:SyntheticEvent<HTMLButtonElement>){
        setTarget(e.currentTarget.name);
        setMainPhoto(photo);
    }

    function handleDeletePhoto(photo:Photo, e:SyntheticEvent<HTMLButtonElement>){
        setTarget(e.currentTarget.name);
        deletePhoto(photo);
    }

    return (
        <Tab.Pane>
            <Grid>
                <Grid.Column width={16}>
                    <Header floated='left' icon='image' content='Photos' />
                    {isCurrentUser && ( // this will only be displayed if the user is viewing his/her own profile
                        <Button floated='right' basic 
                        content={addPhotoMode ? 'Cancel' :  'Add Photo'}
                        onClick={() => setAddPhotoMode(!addPhotoMode)}
                        />
                    )}
                </Grid.Column>
                <Grid.Column width={16}>
                    {addPhotoMode ? (
                        <PhotoUploadWidget uploadPhoto={handlePhotoUpload} loading={uploading} />
                    ): (
                        <Card.Group itemsPerRow={5}>
                        {profile.photos?.map(photo => (
                            <Card key={photo.id}>
                                <Image src={photo.url}/>
                                {isCurrentUser && (
                                    <Button.Group fluid widths={2}>
                                        <Button 
                                            basic
                                            color='green'
                                            content='Main'
                                            // we need this in order for our loading flag to differentiate the buttons
                                            // we have set this one's name to 'main' + photo.id
                                            name={'main' + photo.id}
                                            disabled={photo.isMain}
                                            loading={target === 'main' + photo.id && loading}
                                            // we want to pass the event to the button
                                            onClick={e => handleSetMainPhoto(photo, e)}
                                        />
                                        <Button 
                                            basic 
                                            disabled={photo.isMain} 
                                            color='red' 
                                            icon='trash'
                                            name={photo.id} // we need this in order to set the target for the button
                                            loading={target === photo.id && loading}
                                            onClick={e => handleDeletePhoto(photo, e)}
                                        />
                                    </Button.Group>
                                )}
                            </Card>
                        ))}
                    </Card.Group>
                    )}
                </Grid.Column>
            </Grid>         
        </Tab.Pane>
    )
})
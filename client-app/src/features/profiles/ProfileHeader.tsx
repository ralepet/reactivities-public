import { observer } from 'mobx-react-lite';
import React from 'react';
import { Divider, Grid, Header, Item, Segment, Statistic } from 'semantic-ui-react';
import { Profile } from '../../app/models/profile';
import FollowButton from './FollowButton';

interface Props {
    profile: Profile;
}

// even though we aren't adding a store here, because the profile object is coming from our store, we need to make this component an observer
// otherwise we won't react to any changes in our observable inside this component
export default observer (function ProfileHeader({profile}: Props){
    return(
        <Segment>
            <Grid>
                <Grid.Column width={12}> {/*/ Semantic-ui has 16 grids*/}
                    <Item.Group>
                        <Item>
                            <Item.Image avatar size='small' src={profile.image || '/assets/user.png'}/>
                            <Item.Content verticalAlign='middle'> {/* verticalAlign='middle' -> this aligns it with the image*/ }
                                <Header as='h1' content={profile.displayName}/>
                            </Item.Content>  
                        </Item>   
                    </Item.Group>
                </Grid.Column>
                <Grid.Column width={4}>
                    {/* A statistic can display a value with a label above or below it.*/ }
                    <Statistic.Group widths={2}>
                        <Statistic label='Followers' value={profile.followersCount}/>
                        <Statistic label='Following' value={profile.followingCount}/>
                    </Statistic.Group>
                    <Divider /> {/* This is just a horizonal line*/}
                   <FollowButton profile={profile}/>
                </Grid.Column>
            </Grid>
        </Segment>
    )
})
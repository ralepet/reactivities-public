import { ErrorMessage, Form, Formik } from 'formik';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Button, Header, Label } from 'semantic-ui-react';
import MyTextInput from '../../app/common/form/MyTextInput';
import { useStore } from '../../app/stores/store';

// whenever we use a store we should make the function compoent an oberver, so that we can observe any changes in our store
export default observer (function LoginForm(){ 
    const {userStore} = useStore();
    return (
        <Formik
            initialValues={{email: '', password: '', error: null}} // error: null we nned this to use setErrors
             // {setErrors} is a funcion that allows us to set errors in our form, we get if from Formik
            onSubmit={(values, {setErrors}) => userStore.login(values).catch(error => 
                setErrors({error: error.response.data}))}
        >
            {({handleSubmit, isSubmitting, errors}) => (
                /* className='ui form' will bring styling*/ 
                <Form className='ui form' onSubmit={handleSubmit} autoComplete = 'off'> 
                    <Header as='h2' content='Login to Reactivities' color='teal' textAlign='center'/>
                    <MyTextInput name='email' placeholder='Email'/>
                    <MyTextInput name='password' placeholder='Password' type='password'/>
                    <ErrorMessage 
                        name='error' render={() => 
                        <Label style={{marginBottom: 10}} basic color='red' content={errors.error} />}
                    />
                    <Button loading={isSubmitting} positive content='Login' type='submit' fluid/>
                </Form>
            )}
        </Formik>
    )
})
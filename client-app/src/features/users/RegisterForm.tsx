import { ErrorMessage, Form, Formik } from 'formik';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Button, Header } from 'semantic-ui-react';
import MyTextInput from '../../app/common/form/MyTextInput';
import { useStore } from '../../app/stores/store';
import * as Yup from 'yup'
import ValidationErrors from '../errors/ValidationErrors';

// whenever we use a store we should make the function compoent an oberver, so that we can observe any changes in our store
export default observer (function RegisterForm(){ 
    const {userStore} = useStore();
    return (
        <Formik
            initialValues={{displayName: '', username: '', email: '', password: '', error: null}} // error: null we nned this to use setErrors
             // {setErrors} is a funcion that allows us to set errors in our form, we get if from Formik
            onSubmit={(values, {setErrors}) => userStore.register(values).catch(error => 
                setErrors({error}))} // because {error: error} the key and value have the same name, we can shorcut it to {error}
            validationSchema={Yup.object({
                displayName: Yup.string().required(),
                username: Yup.string().required(),
                email: Yup.string().required().email(),
                password: Yup.string().required(),
            })}
        >
            {({handleSubmit, isSubmitting, errors, isValid, dirty}) => (
                /* className='ui form' will bring styling, we also need to give our form an additional class of error in order to display our validation errors*/ 
                <Form className='ui form error' onSubmit={handleSubmit} autoComplete = 'off'>  
                    <Header as='h2' content='Sign up to Reactivities' color='teal' textAlign='center'/>
                    <MyTextInput name='displayName' placeholder='Display Name'/>
                    <MyTextInput name='username' placeholder='Username'/>
                    <MyTextInput name='email' placeholder='Email'/>
                    <MyTextInput name='password' placeholder='Password' type='password'/>
                    <ErrorMessage 
                        name='error' render={() => 
                        <ValidationErrors errors={errors.error}/>}
                    />
                    <Button disabled={!isValid || !dirty || isSubmitting} 
                        loading={isSubmitting} positive content='Register' type='submit' fluid/>
                </Form>
            )}
        </Formik>
    )
})
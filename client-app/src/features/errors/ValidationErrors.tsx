import React from 'react';
import { Message } from 'semantic-ui-react';

interface Props {
    errors: any;
}

export default function ValidationErrors({ errors }: Props) { // the errors will be passed as Props
    return (
        <Message error>
            {errors && (
                <Message.List>
                    {errors.map((err: any, i: any) => ( // i is the index of the thing that we are looping over
                        <Message.Item key={i}>{err}</Message.Item> // because each element of an array has an index, we are using as the key
                    ))}
                </Message.List>
           )}
        </Message>
    )
}
import { observer } from "mobx-react-lite";
import React from "react";
import { Container, Header, Segment } from "semantic-ui-react";
import { useStore } from "../../app/stores/store";

// because we're getting information from the store here, then we're going to need to make this component an observer
export default observer (function ServerError() {
    const {commonStore} = useStore();
    return(
        <Container>
            <Header as="h1" content="Server Error"/>
            {/* error?.message is called optional chaining, it will display "This is a server error" that we are sedning from the API*/}
            <Header sub as="h5" color="red" content={commonStore.error?.message}/>
            {commonStore.error?.details && // if error.details exist
                <Segment>
                    <Header as="h4" content="Stack trace"  color="teal" />
                    <code style={{marginTop: "10px"}}>{commonStore.error.details}</code>
                </Segment>
            }
        </Container>
    )
})
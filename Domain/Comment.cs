namespace Domain;

public class Comment
{
    public int Id { get; set; }
    public string Body { get; set; }
    public AppUser Author { get; set; }
    public Activity Activity { get; set; }
    //the DateTime of the comments is going to be stored in UTC time, no matter where in the world the comment is created
    public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
}
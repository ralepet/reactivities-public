namespace Domain;
/// <summary>
/// This class is our join  entity for the following feature
/// </summary>
public class UserFollowing
{
    public string ObserverId { get; set; }
    // this is the person who is going to follow another user
    public AppUser Observer { get; set; }
    public string TargetId { get; set; } 
    //this is the person who is being followed
    public AppUser Target { get; set; } 
    
}
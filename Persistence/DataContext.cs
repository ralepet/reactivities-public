using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Persistence
{
    /*A DbContext instance represents a combination of the Unit Of Work and Repository patterns 
    such that it can be used to query from a database and group together changes that will then 
    be written back to the store as a unit. DbContext is conceptually similar to ObjectContext.
    */
    // we don't need to specify the DBSet for our AppUser everything's going to be taken care of
    //by the IdentityDbContext and it's going to scaffold a lot of tables in our database related to identity
    public class DataContext : IdentityDbContext<AppUser>
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        // the name specified here is going to be the name that is used in the database
        // the columns are going to match the attributes
        public DbSet<Activity> Activities { get; set; }

        //  if we want to query this table directly in our code, then we do need to have a DB set.
        // So there will be an occasion where we just want to get an attendee without needing to get the activity object as well.
        public DbSet<ActivityAttendee> ActivityAttendees { get; set; }
        public DbSet<Photo> Photos { get; set; }
        // we are adding the DBSet because we want to query the comments directly
        public DbSet<Comment> Comments { get; set; }
        
        //we will add a DbSet because we need to query this directly
        public DbSet<UserFollowing> UserFollowings { get; set; } 

        // we are overriding the OnModelCreating method from IdentityDbContext, so that we can add additional configurations
        protected override void OnModelCreating(ModelBuilder builder)
        {
            /*So what we get access to here is a model builder class that's got a name of builder.
            And then we pass this builder into the base on model creating class.
            */
            base.OnModelCreating(builder);

            // We are configuring our ActivityAttendee entity
            /*We want to tell it about its primary key.
            We have two properties in our ActivityAttendee class that have ID, but we want
            to make up a primary key that's a combination of both the AppUserId ID and the ActivityId.
            */
            builder.Entity<ActivityAttendee>(x => x.HasKey(aa => new {aa.AppUserId, aa.ActivityId}));

            builder.Entity<ActivityAttendee>()
                .HasOne(u => u.AppUser)
                .WithMany(u => u.Activities)
                .HasForeignKey(aa => aa.AppUserId);
            
             builder.Entity<ActivityAttendee>()
                .HasOne(u => u.Activity)
                .WithMany(u => u.Attendees)
                .HasForeignKey(aa => aa.ActivityId);
             
             // We want to delete all the comments for the activity when an Activity object is deleted
             // We are not going to do this for the AppUser, because we want to keep the Comment even when an AppUser is deleted
             builder.Entity<Comment>()
                 .HasOne(a => a.Activity)
                 .WithMany(c => c.Comments)
                 .OnDelete(DeleteBehavior.Cascade);

             // we want to configure our UserFollowing entity, firstly we need to tell it about its primary key, which is going to be a combination of the ObserverId and TargetId
             // then we want to configure the relationships in the table
             builder.Entity<UserFollowing>(b =>
             {
                 b.HasKey(k => new { k.ObserverId, k.TargetId });

                 b.HasOne(o => o.Observer)
                     .WithMany(f => f.Followings)
                     .HasForeignKey(o => o.ObserverId)
                     .OnDelete(DeleteBehavior.Cascade); 
                 
                 b.HasOne(o => o.Target)
                     .WithMany(f => f.Followers)
                     .HasForeignKey(o => o.TargetId)
                     .OnDelete(DeleteBehavior.Cascade); 
             });
        }
    }
}
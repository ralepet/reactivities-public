using Application.Interfaces;
using Application.Photos;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Infrastructure.Photos;
/// <summary>
/// This class allows the user to access the photos stored in Cloudinary
/// </summary>
public class PhotoAccessor : IPhotoAccessor
{
    // we need to create an instance of Cloudinary in order to use the methods from Cloudinary itself
    private readonly Cloudinary _cloudinary;
    public PhotoAccessor(IOptions<CloudinarySettings> config)
    {
        // we are creating the Cloudinary Account with the configuration values
        var account = new Account(
            config.Value.CloudName,
            config.Value.ApiKey,
            config.Value.ApiSecret
        );
        _cloudinary = new Cloudinary(account);
    }

    public async Task<PhotoUploadResult> AddPhoto(IFormFile file)
    {
        if (file.Length <= 0) return null; // if the filesize is 0, that means that there is no file
        
        // we are using the "using" keyword because we want to dispose of this stream, because it's
        // going to consume memory, as soon as we are finished with this method
        await using var stream = file.OpenReadStream();
        var uploadParams = new ImageUploadParams
        {
            File = new FileDescription(file.FileName, stream),
            // we will let Cloudinary transform this into a square image
            Transformation = new Transformation().Height(500).Width(500).Crop("fill")
        };

        var uploadResult = await _cloudinary.UploadAsync(uploadParams);
        if (uploadResult.Error != null)
        {
            throw new Exception(uploadResult.Error.Message);
        }

        // we created the class PhotoUploadResult
        return new PhotoUploadResult
        {
            PublicId = uploadResult.PublicId,
            Url = uploadResult.SecureUrl.ToString()
        };

    }

    public async Task<string> DeletePhoto(string publicId)
    {
        var deleteParams = new DeletionParams(publicId);
        var result = await _cloudinary.DestroyAsync(deleteParams);
        return result.Result == "ok" ? result.Result : null;
    }
}
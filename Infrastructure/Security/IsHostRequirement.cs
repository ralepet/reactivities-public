using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Infrastructure.Security
{
    public class IsHostRequirement : IAuthorizationRequirement
    { 
    }

    public class IsHostRequirementHandler : AuthorizationHandler<IsHostRequirement>
    {
        private readonly DataContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public IsHostRequirementHandler(DataContext dbContext, 
            IHttpContextAccessor httpContextAccessor)
        {
            // we need _httpContextAccessor to get the root id of the activitythat we're trying
            //to access because we need to get the activity ID so that we can check the attendees 
            //of that activity from our ActivityAttendee join table and seee if this particular user
            // is the host of that activity
            _httpContextAccessor = httpContextAccessor;
            _dbContext = dbContext;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsHostRequirement requirement)
        {
            // we can get the userId from our AuthorizationHandlerContext
            var userId = context.User.FindFirstValue(ClaimTypes.NameIdentifier);

            // the user is not authorized
            if(userId == null) return Task.CompletedTask;

            // because in our root value the id is a string, we need to parse it into a Guid object
            // we are adding optional chaining here to be safe
            var activityId = Guid.Parse(_httpContextAccessor.HttpContext?.Request.RouteValues
                .SingleOrDefault(x => x.Key == "id").Value?.ToString());

            /* If we don't add .AsNoTracking(), then the attendee will stay in memory, 
            even though our handler will have been disposed of because it's a transient it doesn't mean 
            that the entity that we've obtained from entity framework is also going to be disposed.
            This is staying in memory and it's causing a problem when we're editing an Activity because we're only
            sending up the Activity object and in our Edit class we're not getting the related entity
            and we've got an ActivityAttendees object inside the memory for that particular activity.
            It's that combination of things that is making our activity, our ActivityAttendees disappear from the list.
            */
            var attendee = _dbContext.ActivityAttendees
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.AppUserId == userId && x.ActivityId == activityId)
                .Result;

            if(attendee == null) return Task.CompletedTask;

            // if the context.Succeed flag is set, then the user will be authorized to edit the activity
            if(attendee.IsHost) context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
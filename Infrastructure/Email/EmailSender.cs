using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Infrastructure.Email
{
    // We could also create an interface IEmailSender to use this class in other parts in the app
    // However, since we need this only in the AccountController, we will not create the interface at this time
    public class EmailSender
    {
        private readonly IConfiguration _config;
        public EmailSender(IConfiguration config)
        {
            _config = config;
        }

        public async Task SendEmailAsync(string userEmail, string emailSubject, string msg)
        {
            var client = new SendGridClient(_config["Sendgrid:Key"]);
            var message = new SendGridMessage
            {
                // this needs to match what our authorized sender is
                From = new EmailAddress("awesomedev1918@gmail.com", _config["Sendgrid:User"]), //this is the email adress of the sender
                Subject = emailSubject,
                PlainTextContent = msg,
                HtmlContent = msg
            };
            //This is setting who the message is going to
            message.AddTo(new EmailAddress(userEmail));
            message.SetClickTracking(false, false);

            await client.SendEmailAsync(message);
        }
    }
}
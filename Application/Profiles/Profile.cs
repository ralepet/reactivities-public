using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;

namespace Application.Profiles
{
    public class Profile
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Bio { get; set; }
        public string Image { get; set; }
        // we need this for when the current user returns another user's profile if the current user is following that particular user
        public bool Following { get; set; } 
        //the number of users following this user
        public int FollowersCount { get; set; }  
        //the number of user this user is following
        public int FollowingCount { get; set; } 
        public ICollection<Photo> Photos { get; set; } 
    }
}
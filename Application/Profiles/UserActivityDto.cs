using System.Text.Json.Serialization;

namespace Application.Profiles;

/// <summary>
/// This is the Dto object that we are going to display in the profile page of a user
/// </summary>
public class UserActivityDto
{
    public Guid Id { get; set; }
    public string Title { get; set; } 
    public string Category { get; set; } 
    public DateTime Date { get; set; }
    // HostUsername will act as a helper to fish out the filters for the activities that the user is hosting
    // we don't want to return HostUsername to the client, so we are adding JsonIgnore
    [JsonIgnore]
    public string HostUsername { get; set; }
}
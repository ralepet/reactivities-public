using Application.Core;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Followers;

/// <summary>
/// We are going to use this class to follow and unfollow an user
/// </summary>
public class FollowToggle
{
    public class Command : IRequest<Result<Unit>>
    {
        // We only need the target's username, because we can get the observer's username from the jwt authentication token
        public string TargetUsername { get; set; }
    }
    
    public class Handler : IRequestHandler<Command, Result<Unit>>
    {
        private readonly DataContext _context;
        private readonly IUserAccessor _userAccessor;

        // We need DataContext because we are updating the database, and we need the IUserAccessor 
        // to access the currently logged-in user
        public Handler(DataContext context, IUserAccessor userAccessor)
        {
            _context = context;
            _userAccessor = userAccessor;
        }

        public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
        {
            var observer = await _context.Users.FirstOrDefaultAsync(x => x.UserName == _userAccessor.GetUsername());
            
            // // we'll assume that because they are authenticated, that we will have an user, however i'm checking this just to make the warnings go away
            if (observer == null) return null;

            var target = await _context.Users.FirstOrDefaultAsync(x => x.UserName == request.TargetUsername);

            // we need to check if we have a target, because this is a parameter that we are receiving from the client
            if (target == null) return null;

            var following = await _context.UserFollowings.FindAsync(observer.Id, target.Id);

            // if the observer isn't following the target, we'll add a new UserFollowings object
            if (following == null)
            {
                following = new UserFollowing
                {
                    Observer = observer,
                    Target = target
                };

                _context.UserFollowings.Add(following);
            }
            else
            {
                _context.UserFollowings.Remove(following);
            }

            var success = await _context.SaveChangesAsync() > 0;

            return success ? Result<Unit>.Success(Unit.Value) : Result<Unit>.Failure("Failed to update following");

        }
    }
}
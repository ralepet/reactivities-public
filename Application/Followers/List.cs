using Application.Core;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Profile = Application.Profiles.Profile;

namespace Application.Followers;

/// <summary>
/// Returns the list of users that are following the user or the list of users that the current user is following
/// </summary>
public class List
{
    public class Query : IRequest<Result<List<Profile>>>
    {
        //we need this in order to tell the app what we want to return,  a list of followers or do
        // we want to return a list of users that is following that user
        public string Predicate { get; set; }

        // we will use this as an root parameter, so that we got access to the user that we are interested in
        public string Username { get; set; } 
    }
    
    public class Handler : IRequestHandler<Query, Result<List<Profile>>> 
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;

        public Handler(DataContext context, IMapper mapper, IUserAccessor userAccessor)
        {
            _context = context;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }

        public async Task<Result<List<Profile>>> Handle(Query request, CancellationToken cancellationToken)
        {
            var profiles = new List<Profile>();
 
            switch (request.Predicate)
            {
                case "followers":
                    profiles = await _context.UserFollowings.Where(x => x.Target.UserName == request.Username)
                        // we need to select the observers
                        .Select(u => u.Observer)
                        // 1) we need to project the AppUser to the Profile class
                        // 2) because ProjectTo can take params as an object, we are passing it into our configuration
                        .ProjectTo<Profile>(_mapper.ConfigurationProvider, 
                            new {currentUsername = _userAccessor.GetUsername()})
                        .ToListAsync();
                    break;
                case "following":
                    profiles = await _context.UserFollowings.Where(x => x.Observer.UserName == request.Username)
                        .Select(u => u.Target)
                        .ProjectTo<Profile>(_mapper.ConfigurationProvider, 
                            new {currentUsername = _userAccessor.GetUsername()})
                        .ToListAsync();
                    break;
                    
            }

            return Result<List<Profile>>.Success(profiles);
        }
    }
}
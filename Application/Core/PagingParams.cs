namespace Application.Core;

/// <summary>
/// These are the parameters used for paging which include the page number and the page size.
/// </summary>
public class PagingParams
{
    // we don't want the client to set the page size to a high number, so we are going to limit them
    private const int MaxPageSize = 50;
    public int PageNumber { get; set; } = 1;

    private int _pageSize = 10; // this is our default value

    public int PageSize
    {
        get => _pageSize;
        // if the value that is passed as the pageSize is greater than MaxPageSize, we are going to set it to the MaxPageSize
        set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
    }
}
using Microsoft.EntityFrameworkCore;

namespace Application.Core;

/// <summary>
/// This list is going to be used for any type of List in the app, it is going to extend the List class,
/// however it is going to have some pagination properties
/// </summary>
/// <typeparam name="T"></typeparam>
public class PagedList<T> : List<T>
{
    public PagedList(IEnumerable<T> items, int count, int pageNumber, int pageSize)
    {
        CurrentPage = pageNumber;
        // We are going to calculate TotalPages by dividing the total amount of items by the page size
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        PageSize = pageSize;
        TotalCount = count;
        //AddRange -> Adds the elements of the specified collection to the end of the List<T>
        AddRange(items);
    }

    public int CurrentPage { get; set; }
    public int TotalPages { get; set; } 
    public int PageSize  { get; set; }
    public int TotalCount { get; set; }

    // because we are going to receive the list of items before they have ben executed to a list in our db
    public static async Task<PagedList<T>> CreateAsync(IQueryable<T> source, int pageNumber, int pageSize)
    {
        // this is going to give us the number of items before pagination has taken place
        // it will execute against the db
        var count = await source.CountAsync();
        // Skip -> Bypasses a specified number of elements in a sequence and then returns the remaining elements
        // Take -> Returns a specified number of contiguous elements from the start of a sequence
        var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
        return new PagedList<T>(items, count, pageNumber, pageSize);
    }
}
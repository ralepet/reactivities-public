using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Core;
using Application.Interfaces;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Activities
{
    public class Create
    {
        // Commands ususally don't return anything, but it's ok to return if the request has been successfull or not
        // using Mediator Unit as the type means that we are not returning anytnig
        public class Command : IRequest<Result<Unit>>
        {
            public Activity Activity {get; set;}
        }

        // we are validating against the Command class, because it contains the Activity
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                // we are setting the validator for this object
                RuleFor(x => x.Activity).SetValidator(new ActivityValidator());
            }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;

            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                _userAccessor = userAccessor;
                _context = context;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                // we are going to get the user, we can accses the users table from the DBContext,
                // we don't need to bring in the UserManager to get access to a user
                // FirstOrDefaultAsync - Asynchronously returns the first element of a sequence, or a default value if the sequence contains no elements.
                var user = await _context.Users.FirstOrDefaultAsync(x => 
                    x.UserName == _userAccessor.GetUsername());

                var attendee = new ActivityAttendee 
                {
                    AppUser = user,
                    Activity = request.Activity,
                    IsHost = true
                };

                request.Activity.Attendees.Add(attendee);

                // we are adding an activity in the DB context in the memory, we are not sending it to the database
                _context.Activities.Add(request.Activity);

                // because SaveChangesAsync returns an integer, that represent the number of state
                // entries written to the database
                var result = await _context.SaveChangesAsync() > 0;

                if(!result) return Result<Unit>.Failure("Failed to create activity");

                // Unit.Value is effectively nothing, but it's going to notify our API controller, 
                // that the handler method was successful
                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}

using Application.Core;

namespace Application.Activities;

/// <summary>
/// These are the parameters that we send when we want to filter the list of Activities, it includes the PagingParams
/// as well as some properties unique to the Activity class
/// </summary>
public class ActivityParams : PagingParams
{
    public bool isGoing { get; set; }
    public bool isHost { get; set; }
    public DateTime StartDate { get; set; } = DateTime.UtcNow;
}
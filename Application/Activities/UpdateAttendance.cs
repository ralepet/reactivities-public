using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Core;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Activities
{
    public class UpdateAttendance
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                _userAccessor = userAccessor;
                _context = context;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                // we are eagerly loading the related data, firstly we are including the join table, then we are including the user table from the DB
                var activity = await _context.Activities
                    .Include(a => a.Attendees).ThenInclude(u => u.AppUser)
                    .SingleOrDefaultAsync(x => x.Id == request.Id);

                // this will result in a 404 sent back to the client
                if (activity == null) return null;

                // we are getting the user that is invoking the http method from the API
                var user = await _context.Users.FirstOrDefaultAsync(x => 
                    x.UserName == _userAccessor.GetUsername());

                // this will result in a 404 sent back to the client
                if(user == null) return null;

                // this is not an asny method because we got the activity as well as the atendees in
                // memory at this stage
                // we will ad the optinal chaining to both the isHost and AppUser
                var hostUserName = activity.Attendees.FirstOrDefault(x => x.IsHost)?.AppUser?.UserName;

                // this is the attendance status for this particular user
                var attendance = activity.Attendees.FirstOrDefault(x => x.AppUser.UserName == user.UserName);

                // this means that the host is cancelling the activity
                if (attendance != null && hostUserName == user.UserName )
                    activity.IsCancelled = !activity.IsCancelled;

                // this is just a normal attendee, we will remove them from the attendees list
                if (attendance != null && hostUserName != user.UserName )
                    activity.Attendees.Remove(attendance);

                if(attendance == null)
                {
                    attendance = new ActivityAttendee
                    {
                        AppUser = user,
                        Activity = activity,
                        IsHost = false
                    };

                    activity.Attendees.Add(attendance);
                }

                // SaveChangesAsync -> Saves all changes made in this context to the database. It will return the number of state entries written to the database.
                var result = await _context.SaveChangesAsync() > 0;

                // if the result is true 
                return result ? Result<Unit>.Success(Unit.Value) : Result<Unit>.Failure("Problem updating attendance");
            }
        }
    }
}
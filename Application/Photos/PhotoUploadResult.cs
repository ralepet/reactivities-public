namespace Application.Photos;

public class PhotoUploadResult
{
    //these 2 properties we are getting from Cloudinary's UploadResult class
    public string PublicId { get; set; }
    public string Url { get; set; }
}
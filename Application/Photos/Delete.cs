using Application.Core;
using Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Photos;

public class Delete
{
    // Unit represents a void type, we are using it since we aren't returning anything
    public class Command : IRequest<Result<Unit>>
    {
        //This Id matches the public Id that we need to pass to Cloudinary in order to delete the photo from Cloudinary
        public string Id { get; set; }
    }
    
    public class Handler : IRequestHandler<Command, Result<Unit>>
    {
        private readonly DataContext _context;
        private readonly IPhotoAccessor _photoAccessor;
        private readonly IUserAccessor _userAccessor;

        public Handler(DataContext context, IPhotoAccessor photoAccessor, IUserAccessor userAccessor)
        {
            _context = context;
            _photoAccessor = photoAccessor;
            _userAccessor = userAccessor;
        }

        public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
        {
            // Firstly, we are going to get the currently logged-in user from the database
            // We are using eager loading because we need to get access to the user's photo collection
            var user = await _context.Users.Include(p => p.Photos)
                .FirstOrDefaultAsync(x => x.UserName == _userAccessor.GetUsername());
            if (user == null) return null;
            
            // we aren't using async here because we have already retrieved the user from the DB, and we have included the photos
            // from the photo collection
            var photo = user.Photos.FirstOrDefault(x => x.Id == request.Id);
            
            // if the photo doesn't exist in the Photos collection of the current user
            if (photo == null) return null;
            
            // because we are using the main photo in the entire app, we are going to tell the user to change their main
            // photo before we are going to allow them to delete it
            if (photo.IsMain) return Result<Unit>.Failure("You cannot delete your main photo");
            
            // This will return the public id of the photo if the removal from Cloudinary is successful
            var result = await _photoAccessor.DeletePhoto(photo.Id);

            if (result == null) return Result<Unit>.Failure("Problem deleting photo from Cloudinary");

            // we are removing the photo from the user's Photos collection
            user.Photos.Remove(photo);

            // we are removing the photo from the database
            var success = await _context.SaveChangesAsync() > 0;

            return success
                ? Result<Unit>.Success(Unit.Value)
                : Result<Unit>.Failure("Problem deleting photo from API");
        }
    }
}
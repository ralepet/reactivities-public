using Application.Core;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Photos;

public class Add
{
    public class Command : IRequest<Result<Photo>>
    {
        public IFormFile File { get; set; }
    }
    
    public class Handler : IRequestHandler<Command, Result<Photo>>
    {
        private readonly DataContext _context;
        private readonly IPhotoAccessor _photoAccessor;
        private readonly IUserAccessor _userAccessor;

        public Handler(DataContext context, IPhotoAccessor photoAccessor, IUserAccessor userAccessor)
        {
            // we need the context to save the photo Url to the database
            _context = context;
            _photoAccessor = photoAccessor;
            _userAccessor = userAccessor;
        }

        public async Task<Result<Photo>> Handle(Command request, CancellationToken cancellationToken)
        {
            // Firstly, we are going to get the currently logged-in user from the database
            // We are using eager loading because we need to get access to the user's photo collection
            var user = await _context.Users.Include(p => p.Photos)
                .FirstOrDefaultAsync(x => x.UserName == _userAccessor.GetUsername());
            if (user == null) return null;

            // if this doesn't work, the logic inside the _photoAccessor is going to throw an exception, so we don't need to check if this works
            var photoUploadResult = await _photoAccessor.AddPhoto(request.File);

            var photo = new Photo
            {
                Url = photoUploadResult.Url,
                Id = photoUploadResult.PublicId
            };

            // if the user doesn't have any photos that are set to main, then we are going to set the current photo to be the main
            if (!user.Photos.Any(x => x.IsMain)) photo.IsMain = true;
            // we are adding the photo to the Photos collection
            user.Photos.Add(photo);
            
            // we are saving the changes to the database
            var result = await _context.SaveChangesAsync() > 0;
            
            return result ? Result<Photo>.Success(photo) : Result<Photo>.Failure("Problem adding photo");
        }
    }
}
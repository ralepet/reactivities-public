using Application.Core;
using Application.Interfaces;
using AutoMapper;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Comments;

/// <summary>
/// This is the Create handler for the CommentDto
/// </summary>
public class Create
{
    public class Command : IRequest<Result<CommentDto>>
    {
        public string Body { get; set; }
        public Guid ActivityId { get; set; } 
    }
    
    // because we don't want an empty comment, we will specify that the body of the comment cannot be empty
    public class CommandValidator : AbstractValidator<Command>
    {
        public CommandValidator()
        {
            RuleFor(x => x.Body).NotEmpty();
        }
    }
    
    public class Handler : IRequestHandler<Command,Result<CommentDto>>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;

        public Handler(DataContext context, IMapper mapper, IUserAccessor userAccessor)
        {
            _context = context;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }

        // because we need our server to generate an Id for our Comment, we will return the CommentDto instead of Unit(that is void type)
        public async Task<Result<CommentDto>> Handle(Command request, CancellationToken cancellationToken)
        {
            var activity = await _context.Activities.FindAsync(request.ActivityId);

            if (activity == null) return null;

            // Because we want to populate the Image property inside of the CommentDto, we will need the Photo collection from our user
            var user = await _context.Users
                .Include(p => p.Photos)
                .SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetUsername());

            // because CreatedAt is always instantiated to DateTime.UtcNow, we don't need to specify it here
            var comment = new Comment()
            {
                Author = user,
                Activity = activity,
                Body = request.Body
            };
            
            // this will add the Comment in the collection for that Activity
            activity.Comments.Add(comment);

            var success = await _context.SaveChangesAsync() > 0;

            return success ? Result<CommentDto>.Success(_mapper.Map<CommentDto>(comment)) : Result<CommentDto>.Failure("Failed to add comment");
        }
    }
}